import React, {Component} from 'react';
import './App.css';
import NavBar from './components/NavBar';
import {BrowserRouter as Router, Route, Link, Switch} from "react-router-dom";
import SalesOrderList from './components/salesorders/SalesOrderList';
import SalesOrderDetails from './components/salesorders/SalesOrderDetails';
import ProductionOrderList from './components/productionorders/ProductionOrderList';
import ProductionOrderDetails from './components/productionorders/ProductionOrderDetails';
import InspectionLists from './components/inspections/InspectionLists';
import RejectionNoticeForm from './components/rejectionnotices/RejectionNoticeForm';
import RejectionNoticeDetails from './components/rejectionnotices/RejectionNoticeDetails';
import CapacityPlan from './components/capacityplan/CapacityPlan';
import WorkScheduleForm from './components/workschedules/WorkScheduleForm';
import NavDrawer from './components/NavDrawer';
import {AuthRoute, PRODUCTION_INSPECTOR} from "./components/auth/AuthRoute";
import Login from "./components/auth/Login";
import {getAuthToken} from "./components/auth/SessionManager";
import axios from "axios";

class App extends Component {
    state = {
        sideDrawerOpen: false,
        isMenuOpen: false,
    }

    drawerToggleClickHandler = () => {
        this.setState(prevState => {
            return {sideDrawerOpen: !prevState.sideDrawerOpen}
        });
    }

    isMenuOpen = (state) => {
        this.setState({isMenuOpen: state.isOpen});
    }

    render() {
        if (getAuthToken() !== '') {
            axios.defaults.headers.common = {'Authorization': 'Bearer ' + getAuthToken()};
        }
        return (
            <div>
                <NavBar click={this.drawerToggleClickHandler} open={this.state.isMenuOpen}/>
                <NavDrawer show={this.state.sideDrawerOpen} app={this.isMenuOpen}/>
                <div className="container-fluid">
                    <div className="main py-4">
                        <Router>
                            <Switch>
                                <Route path={"/login"} component={Login} exact/>
                                <AuthRoute path={"/salesorders"} component={SalesOrderList} exact/>
                                <AuthRoute path={"/salesorders/:salesOrderId"} component={SalesOrderDetails} exact/>

                                <AuthRoute path={"/productionorders"} component={ProductionOrderList} exact/>
                                <AuthRoute path={"/productionorders/:productionOrderId"}
                                           component={ProductionOrderDetails} exact/>
                                <AuthRoute path={"/productionorders/:productionOrderId/reject"}
                                           component={PRODUCTION_INSPECTOR(RejectionNoticeForm)} exact/>
                                <AuthRoute path={"/inspections"} component={InspectionLists} exact/>
                                <AuthRoute path={"/inspections/:inspectionId/rejectionNotice"}
                                           component={RejectionNoticeDetails} exact/>

                                <AuthRoute path={"/capacityplan"} component={CapacityPlan} exact/>
                                <AuthRoute path={"/capacityplan/workschedule"} component={WorkScheduleForm} exact/>
                            </Switch>
                        </Router>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
