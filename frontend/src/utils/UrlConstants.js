class UrlConstants {
    static get SERVER_HOST() {
        return "http://localhost";
    }
    
    static get SERVER_PORT() {
        return ":8000";
    }
    
    static get API_ROUTE() {
        return "/api/";
    }
    
    static get BASE_URL() {
        return this.SERVER_HOST + this.SERVER_PORT + this.API_ROUTE;
    }

    static get OAUTH_TOKEN_ROUTE() {
        return this.BASE_URL + "oauth/token";
    }

    static get EMPLOYEES() {
        return this.BASE_URL + "employees";
    }

    static EMPLOYEE($id) {
        return this.EMPLOYEES + "/" + $id;
    }

    static AVAILABLE_WORK_HOURS($id, $date) {
        return this.EMPLOYEE($id) + "/availableworkhours/" + $date;
    }

    static get WORK_SCHEDULES() {
        return this.BASE_URL + "workscheduleitems";
    }

    static WORK_SCHEDULE($id) {
        return this.WORK_SCHEDULES + "/" + $id;
    }

    static get SALES_ORDERS() {
        return this.BASE_URL + "orders";
    }
    
    static SALES_ORDER($id) {
        return this.SALES_ORDERS + "/" + $id;
    }
    
    static get PRODUCTION_ORDERS() {
        return this.BASE_URL + "orderitems";
    }
    
    static PRODUCTION_ORDER($id) {
        return this.PRODUCTION_ORDERS + "/" + $id;
    }

    static REMAINING_PRODUCTION_HOURS($id) {
        return this.PRODUCTION_ORDER($id) + "/remaininghours";
    }
    
    static get BILL_OF_MATERIALS() {
        return this.BASE_URL + "billofmaterials";
    }
    
    static BILL_OF_MATERIAL($id) {
        return this.BILL_OF_MATERIALS + "/" + $id;
    }
    
    static get BILL_ITEMS() {
       return this.BASE_URL + "billitems";
    }
    
    static BILL_ITEM($id) {
        return this.BILL_ITEMS + "/" + $id;
    }
    
    static get INSPECTIONS() {
        return this.BASE_URL + "inspections";
    }
    
    static INSPECTION($id) {
        return this.INSPECTIONS + "/" + $id;
    }
    
    static get REJECTION_NOTICES() {
        return this.BASE_URL + "rejectionnotices";
    }
    
    static REJECTION_NOTICE($id) {
        return this.REJECTION_NOTICES + "/" + $id;
    }
}

export default UrlConstants;