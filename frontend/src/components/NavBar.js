import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import logo from '../logo.svg';
import {deleteSession, getAuthToken} from "./auth/SessionManager";

const logout = () => {
    deleteSession();
}

const NavBar = props => {
    return (
        <Navbar bg="primary" expand="lg" variant="dark">
        <div className={props.open ? "burger-menu open" : "burger-menu"} onClick={props.click}>
            <div className="bar1" key="b1" />
            <div className="bar2" key="b2" />
            <div className="bar3" key="b3" />
        </div>
        <Navbar.Brand href="/">
          <img
            alt=""
            src={ logo }
            width="30"
            height="30"
            className="d-inline-block align-top"
          />
          {' Production Planning '}
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
            </Nav>
            <Nav>
                {getAuthToken() === '' ? (<Nav.Link href="/login">Login</Nav.Link>) : (<Nav.Link onClick={logout} href="/login">Logout</Nav.Link>)}
            </Nav>
        </Navbar.Collapse>
        </Navbar>
    )
};

export default NavBar;