import React, { Component } from 'react';
import axios from 'axios';
import UrlConstants from '../../utils/UrlConstants';
import SalesOrderRow from './SalesOrderRow';
import Table from 'react-bootstrap/Table';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { ClipLoader } from 'react-spinners';

class SalesOrderList extends Component{
    constructor() {
        super();
        
        this.state = {
            orders: [],
            isLoading: false,
        }
    }

    componentDidMount() {
        this.setState({isLoading:true});
        this.getOrders();
    }

    renderOrders() {
        const {orders} = this.state;
         return orders.map(order => {
            return (
                <tr key={order.id}>
                    <SalesOrderRow order={order} />
                </tr>
            );
        })
    }

    render() {
        if(this.state.isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>
            );
        }
        
        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Sales Orders</Breadcrumb.Item>
                </Breadcrumb>
                <h1>Sales Orders</h1>
                <Table hover variant="bg-white">
                    <thead>
                        <tr>
                            <th scope="col">Order No</th>
                            <th scope="col">Due Date</th>
                            <th scope="col">Projected Completion Date</th>
                            <th scope="col">Status</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderOrders()}
                    </tbody>
                </Table>
            </div>
        );
    }
    
    getOrders = () => {
        axios.get(UrlConstants.SALES_ORDERS)
            .then(res => this.setState({orders: res.data, isLoading: false}))
    };
}

export default SalesOrderList;