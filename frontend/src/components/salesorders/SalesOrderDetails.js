import React, { Component } from 'react';
import axios from 'axios';
import { ClipLoader } from 'react-spinners';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Alert from 'react-bootstrap/Alert';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import UrlConstants from '../../utils/UrlConstants';
import SalesOrderCard from './SalesOrderCard';
import ProductionOrderRow from '../productionorders/ProductionOrderRow';

class SalesOrderDetails extends Component {
    constructor() {
        super();
        
        this.state = {
            salesOrder: {},
            productionOrders: [],
            isLoading: true,
        }
    }
    
    componentDidMount() {
        this.getOrder();
        this.getProductionOrders();
    }
    
    renderProductionOrders() {
        return this.state.productionOrders.map(productionOrder => {
            return (
                <tr key={productionOrder.id}>
                    <ProductionOrderRow order={productionOrder} />
                </tr>
            );
        })
    }
    
    render() {
        if(this.state.isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader 
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>
            );
        }
        
        return (
            <React.Fragment>
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item href="/salesorders">Sales Orders</Breadcrumb.Item>
                    <Breadcrumb.Item active>{this.state.salesOrder.id}</Breadcrumb.Item>
                </Breadcrumb>
                <SalesOrderCard salesOrder={this.state.salesOrder} />
                <Card>
                    <div className="card-header">
                        <h2>Production Orders</h2>
                    </div>
                    <div className="card-body">
                        <Table hover variant="bg-white">
                            <thead>
                                <tr>
                                    <th scope="col">Order No</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Product ID</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.renderProductionOrders()}
                            </tbody>
                        </Table>
                    </div>
                </Card>
            </React.Fragment>
        );
    }
    
    getOrder = () => {
        let orderId = this.props.match.params.salesOrderId;
        axios.get(UrlConstants.SALES_ORDER(orderId))
            .then(res => this.setState({salesOrder: res.data, isLoading:false}))
            .catch(error => {
                console.error(error);
                if(error.response.status === 404) {
                    var message = 'No such production order found!';
                    this.setState({errors: [...this.state.errors, message]});
                }
            });
    }
    
    getProductionOrders = () => {
        let orderId = this.props.match.params.salesOrderId;
        axios.get(UrlConstants.PRODUCTION_ORDERS + "?order_id=" + orderId + "&includes=order,orderItemStatus")
            .then(res=> this.setState({productionOrders: res.data, isLoading:false}));
    }
    
    renderErrors = () => {
        const { errors } = this.state;
        return Object.keys(errors).map((key, i) => {
            return (
                <Alert key={key} variant="danger">
                    <Alert.Heading>Something's not right</Alert.Heading>
                    <p>
                        {errors[key]}
                    </p>
                </Alert>
            )
        });
    }
}

export default SalesOrderDetails;