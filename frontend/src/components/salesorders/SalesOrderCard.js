import React from 'react';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import StatusBadge from '../StatusBadge';

const SalesOrderCard = ({salesOrder}) => {
    return (
        <Card>
            <div className="card-header">
                <h2>Sales Order #{salesOrder.id}</h2>
            </div>
            <div className="card-body">
                <Table>
                <tbody>
                    <tr>
                        <th scope="column">Number</th>
                        <td>{salesOrder.id}</td>
                    </tr>
                    <tr>
                        <th scope="column">Due Date</th>
                        <td>{salesOrder.due_date}</td>
                    </tr>
                    <tr>
                        <th scope="column">Projected Completion Date</th>
                        <td>{salesOrder.projected_completion_date}</td>
                    </tr>
                    <tr>
                        <th scope="column">Status</th>
                        <td><StatusBadge status={salesOrder.order_status.status} /></td>
                    </tr>
                </tbody>
                </Table>
            </div>
        </Card>
    );
}

export default SalesOrderCard;