import React from 'react';
import StatusBadge from '../StatusBadge';

const SalesOrderRow = ({order}) => {
    return (
        <React.Fragment>
        <th scope="row">{order.id}</th>
        <td>{order.due_date}</td>
        <td>{order.projected_completion_date}</td>
        <td><StatusBadge status={order.order_status.status} /></td>
        <td className="text-center"><a className="btn btn-primary" href={"/salesorders/" + order.id}>View Order</a></td>
        </React.Fragment>
    );
}

export default SalesOrderRow;