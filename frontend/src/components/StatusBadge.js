import React from 'react';
import Badge from 'react-bootstrap/Badge';

//TODO Add all statuses
/** Stateless component or pure component **/
const StatusBadge = ({status}) => {    
    if(status === "Enquiry") {
        return (<Badge pill variant="primary">{status}</Badge>);
    } 
    else if(status === "New") {
        return (<Badge pill variant="info">{status}</Badge>);
    }
    else if(status === "Cancelled") {
        return (<Badge pill variant="light">{status}</Badge>);
    }
    else if(status === "In Progress") {
        return (<Badge pill variant="dark">{status}</Badge>);
    }
    else if(status === "Complete" || status === "Approved") {
        return (<Badge pill variant="success">{status}</Badge>);
    }
    else if(status === "Awaiting Inspection") {
        return (<Badge pill variant="warning">{status}</Badge>);
    }
    else if(status === "Pending") {
        return (<Badge pill variant="warning">{status}</Badge>);
    }
    else if(status === "Rejected") {
        return (<Badge pill variant="danger">{status}</Badge>);
    }
    else {
        return (<Badge pill variant="secondary">{status}</Badge>)
    }
}

export default StatusBadge;