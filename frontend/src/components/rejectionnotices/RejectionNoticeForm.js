import React, { Component } from 'react';
import axios from 'axios';
import { ClipLoader } from 'react-spinners';
import UrlConstants from '../../utils/UrlConstants';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { AlertList } from "react-bs-notifier";
import ProductionOrderCard from '../productionorders/ProductionOrderCard';
import moment from 'moment/moment';
import {getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class RejectionNoticeForm extends Component {
    constructor() {
        super();
        
        this.state = {
            productionOrder: {},
            isLoading: true,
            validated: false,
            rejectionCause: '',
            alerts: []
        }
        
        this.handleRejectionCauseChange = this.handleRejectionCauseChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleRejectionCauseChange(event) {
        this.setState({rejectionCause: event.target.value});
    }
    
    handleSubmit(event) {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            event.preventDefault();
            this.rejectOrder();
        }
        this.setState({ validated: true });
    }
    
    onAlertDismissed = (alert) => {
        const alerts = this.state.alerts;
        
        const idx = alerts.indexOf(alert);
        if(idx >= 0) {
            this.setState({alerts: [...alerts.slice(0, idx), ...alerts.slice(idx+1)]});
        }
    }
    
    componentDidMount() {
        this.getOrder();
    }

    render() {
        var alerts = this.renderAlerts();
        var currentDate = moment().format('DD/MM/YYYY');
        const { productionOrder, validated } = this.state;
        
        if(this.state.isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader 
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>);
        }
        
        if(productionOrder.order_item_status.status !== 'Awaiting Inspection') {
            return(
                <React.Fragment>
                    <Alert variant="danger">
                        <Alert.Heading>Cannot reject this order!</Alert.Heading>
                        <p>
                            You can only reject an order which is Awaiting Inspection.
                        </p>
                    </Alert>
                    <ProductionOrderCard productionOrder={productionOrder} />
                </React.Fragment>
            );
        }
        
        return (
            <React.Fragment>
                {alerts}
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item href="/productionorders">Production Orders</Breadcrumb.Item>
                    <Breadcrumb.Item href={"/productionorders/" + productionOrder.id}>{productionOrder.id}</Breadcrumb.Item>
                    <Breadcrumb.Item active>Create Rejection Notice</Breadcrumb.Item>
                </Breadcrumb>
                <Card>
                    <div className="card-header">
                        <h2>Production Order #{productionOrder.id} Rejection Notice</h2>
                    </div>
                    <div className="card-body">
                        <Form
                            noValidate
                            validated={validated}
                            onSubmit={this.handleSubmit}
                        >
                            <Form.Group as={Row} controlId="rejection_date">
                                <Form.Label column sm={1}>
                                    Rejection Date
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="text" disabled value={currentDate} ref={(element) => {this.input = element}} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="production_order">
                                <Form.Label column sm={1}>
                                    Production Order
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="text" value={productionOrder.id + " " + productionOrder.product.name} disabled />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="quantity">
                                <Form.Label column sm={1}>
                                    Quantity
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="number" value={productionOrder.quantity} disabled />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="rejection_cause">
                                <Form.Label column sm={1}>
                                    Rejection Cause
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control as="textarea" rows="5" placeholder="Rejection Cause" onChange={this.handleRejectionCauseChange} required />
                                    <Form.Control.Feedback type="invalid">Rejection cause cannot be empty.</Form.Control.Feedback>
                                </Col>
                            </Form.Group>
                            <Button variant="danger" type="submit">Reject</Button>
                        </Form>
                    </div>
                </Card>
            </React.Fragment>
        );
    }
    
    getOrder = () => {
        let orderId = this.props.match.params.productionOrderId;
        axios.get(UrlConstants.PRODUCTION_ORDER(orderId))
            .then(res => this.setState({productionOrder: res.data, isLoading: false}));
    }
    
    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }
        
        this.setState({alerts: [...this.state.alerts, newAlert]});
    }

    rejectOrder = () => {
        axios.post(UrlConstants.REJECTION_NOTICES, {
            inspector_id: getSessionValue(USER_ID),
            rejection_cause: this.state.rejectionCause,
            inspection_result: "Rejected",
            order_item_id: this.state.productionOrder.id,
            inspection_date: moment().format('YYYY-MM-DD')
        })
        .then(response => {
            this.generateAlert("success", "Success", "Rejection notice created successfully!");
            //Wait 3 seconds before redirecting user
            setTimeout(function() {
                window.location.href = "/inspections/";
            }, 3000);
        })
        .catch(error => {
            console.error(error);
            if(error.response && error.response.status === 422) {
                var errors = error.response.data.error.message;
                Object.keys(errors).map((key, i) => {
                    return this.generateAlert("danger", "Whoa, something's not right!", errors[key]);
                });
            }
            if(error.response && error.response.status === 500) {
                this.generateAlert("danger", "Whoa, something's not right!", "Server Error");
            }
        });
    }
    
    
    renderAlerts = () => {
        return (
            <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed} alerts={this.state.alerts} />
        );
    }
    
}

export default RejectionNoticeForm;