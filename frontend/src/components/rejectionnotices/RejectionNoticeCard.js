import React, { Component } from 'react';
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { AlertList } from 'react-bs-notifier';
import UrlConstants from '../../utils/UrlConstants';
import {getRole} from "../auth/SessionManager";

class RejectionNoticeCard extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            action: '',
            validated: false,
            alerts: []
        }
    }
    
    handleActionChange = (event) => {
        this.setState({action: event.target.value});
    }
    
    handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            event.preventDefault();
            this.updateRejectionNotice();
        }
        this.setState({ validated: true });
    }
    
    onAlertDismissed = (alert) => {
        const alerts = this.state.alerts;
        
        const idx = alerts.indexOf(alert);
        if(idx >= 0) {
            this.setState({alerts: [...alerts.slice(0, idx), ...alerts.slice(idx+1)]});
        }
    }
    
    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }
        
        this.setState({alerts: [...this.state.alerts, newAlert]});
    }
    
    renderAlerts = () => {
        return (
            <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed} alerts={this.state.alerts} />
        );
    }
    
    render() {
        let alerts = this.renderAlerts();
        const { validated } = this.state;
        let actionUpdatable;
        // Textbox will be displayed to update the rejection action if the rejection 
        // notice has not been responded to by the production manager 
        if(this.props.rejectionNotice.action === null && ['ADMIN', 'PI'].some(role => role === getRole())) {
            actionUpdatable = 
                <>
                    <tr>
                        <th scope="column">Action</th>
                        <td><Form.Control type="text" onChange={this.handleActionChange} required /><Form.Control.Feedback type="invalid">Action cannot be empty.</Form.Control.Feedback></td>
                    </tr>
                    <tr><td><Button variant="info" type="submit">Update Rejection Notice</Button></td></tr>
                </>
        } else {
            actionUpdatable =
                <tr>
                    <th scope="column">Action</th>
                    <td>{this.props.rejectionNotice.action}</td>
                </tr>
        }
        
        return (
            <Card>
                {alerts}
                <div className="card-header">
                    <h2>Rejection Notice #{this.props.rejectionNotice.id}</h2>
                </div>
                <div className="card-body">
                    <Form
                        noValidate
                        validated={validated}
                        onSubmit={this.handleSubmit}
                    >
                        <Table>
                        <tbody>
                            <tr>
                                <th scope="column">Number</th>
                                <td>{this.props.rejectionNotice.id}</td>
                            </tr>
                            <tr>
                                <th scope="column">Rejection Cause</th>
                                <td>{this.props.rejectionNotice.rejection_cause}</td>
                            </tr>
                            {actionUpdatable}
                        </tbody>
                        </Table>
                    </Form>
                </div>
            </Card>
        );
    }

    updateRejectionNotice = () => {
        var rejectionNotice = this.props.rejectionNotice;
        axios.put(UrlConstants.REJECTION_NOTICE(rejectionNotice.id), {
            action: this.state.action
        })
        .then(response => {
            this.generateAlert("success", "Success", "Rejection notice updated!");
            //Wait 3 seconds before reloading page
            setTimeout(function() {
                window.location.reload();
            }, 3000);
        })
        .catch(error => {
            if(error.response && error.response.status === 422) {
                var errors = error.response.data.error.message;
                Object.keys(errors).map((key, i) =>{
                    return this.generateAlert("danger", "Whoa, something's not right!", errors[key]);
                });
            }
            if(error.response && error.response.status === 500) {
                this.generateAlert("danger", "Whoa, something's not right!", "Server Error");
            }
        })
    }
}

export default RejectionNoticeCard;