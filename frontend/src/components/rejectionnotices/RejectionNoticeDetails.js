import React, { Component } from 'react';
import axios from 'axios';
import { ClipLoader } from 'react-spinners';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import UrlConstants from '../../utils/UrlConstants';
import InspectionCard from '../inspections/InspectionCard';
import Alert from 'react-bootstrap/Alert';
import RejectionNoticeCard from './RejectionNoticeCard';

class RejectionNoticeDetails extends Component {
    constructor() {
        super();
        
        this.state = {
            inspection: {},
            rejectionNotice: {},
            isLoading: true,
            errors: []
        }
    }
    
    componentDidMount() {
        this.getInspection();
        this.getRejectionNotice();
    }
    
    render() {
        if(this.state.isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader 
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>);
        }
        
        if(this.state.errors.length !== 0) {
            return (<React.Fragment>{this.renderErrors()}</React.Fragment>);
        }
        
        return (
            <React.Fragment>
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item href="/inspections">Inspections</Breadcrumb.Item>
                    <Breadcrumb.Item active>Rejection Notice #{this.state.rejectionNotice.id}</Breadcrumb.Item>
                </Breadcrumb>
                <InspectionCard inspection={this.state.inspection} />
                <RejectionNoticeCard rejectionNotice={this.state.rejectionNotice} />
            </React.Fragment>
        );
    }
    
    getInspection = () => {
        let inspectionId = this.props.match.params.inspectionId;
        axios.get(UrlConstants.INSPECTION(inspectionId))
            .then(res => this.setState({inspection: res.data}))
            .catch(error => {
                console.error(error);
                if(error.response.status === 404) {
                    var message = 'No such inspecion found!';
                    this.setState({errors: [...this.state.errors, message], isLoading:false});
                }
            })
    }
    
    getRejectionNotice = () => {
        let inspectionId = this.props.match.params.inspectionId;
        axios.get(UrlConstants.REJECTION_NOTICES + "?inspection_id=" + inspectionId)
            .then(res=>this.setState({rejectionNotice: res.data[0], isLoading:false}));
    }
    
    renderErrors = () => {
        const { errors } = this.state;
        return Object.keys(errors).map((key, i) => {
            return (
                <Alert key={key} variant="danger">
                    <Alert.Heading>Something's not right</Alert.Heading>
                    <p>
                        {errors[key]}
                    </p>
                </Alert>
            )
        });
    }
}

export default RejectionNoticeDetails;