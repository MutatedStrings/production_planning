import React, {Component} from 'react';
import axios from 'axios';
import moment from 'moment';
import {ClipLoader} from 'react-spinners';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import UrlConstants from '../../utils/UrlConstants';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import {AlertList} from 'react-bs-notifier';
import Slider from 'react-rangeslider';

class WorkScheduleForm extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            alerts: [],
            validated: false,
            estimatedCompletionHours: 0,
            plannedHours: 0,
            workScheduleItem: {},
            employees: [],
            orders: [],
            horizontal: 0,
            selectedEmployee: {},
            availableWorkHours: 9,
            isLoading: true,
        }
    }
    
    onAlertDismissed = (alert) => {
        const alerts = this.state.alerts;
        
        const idx = alerts.indexOf(alert);
        if(idx >= 0) {
            this.setState({alerts: [...alerts.slice(0, idx), ...alerts.slice(idx+1)]});
        }
    }

    handleChangeHorizontal = value => {
        let end = moment(this.state.workScheduleItem.start, "HH:mm").add(this.state.horizontal, 'hours');
        end = moment(end).format("HH:mm");
        this.setState({
            horizontal: value,
            workScheduleItem: {...this.state.workScheduleItem, end: end}
        })
    };

    handleChange = (event) => {
        const {workScheduleItem} = this.state;
        const {id, value} = event.target;

        if(id === 'order_item_id') {
            this.getProductRemainingHours(value);
        }

        if(id === 'employee_id') {
            this.getAvailableWorkHours(value, this.state.workScheduleItem.date);
        }

        this.setState({workScheduleItem: {...workScheduleItem, [id]: value}});
    }
    
    handleSubmit = (event) => {
        const form = event.currentTarget;
        const {workScheduleItem, horizontal} = this.state;
        if(horizontal === 0) {
            this.generateAlert("danger", "Warning", "Duration cannot be 0");
        }

        if(workScheduleItem.start === workScheduleItem.end) {
            this.generateAlert("danger", "Warning", "Start and end cannot be the same");
        }

        if(form.checkValidity() === false || horizontal === 0 || workScheduleItem.start === workScheduleItem.end) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            event.preventDefault();
            this.createWorkScheduleItem();
        }
        this.setState({validated: true});
    }
    
    componentDidMount() {
        if(this.props.location.state === undefined) {
            this.generateAlert("warning", "Warning", "All values not set!");
        } else {
            let { start, end, employeeId } = this.props.location.state;
            let date = moment(start).format("YYYY-MM-DD");
            start = moment(start).format("HH:mm");
            end = moment(end).format("HH:mm");
            this.setState({workScheduleItem: {...this.state.workScheduleItem,
                    employee_id: employeeId,
                    start: start,
                    end: end,
                    date: date
                }}, () => this.getAvailableWorkHours(this.state.workScheduleItem.employee_id, this.state.workScheduleItem.date));
        }
        this.getEmployees();
        this.getOrders();
    } 
    
    render() {
        let alerts = this.renderAlerts();
        const { validated, employees, workScheduleItem, horizontal, availableWorkHours, orders, estimatedCompletionHours, plannedHours } = this.state;

        if(this.state.isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>
            )
        }

        return(
            <React.Fragment>
                {alerts}
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item href="/capacityplan">Capacity Plan</Breadcrumb.Item>
                    <Breadcrumb.Item active>Create Work Schedule</Breadcrumb.Item>
                </Breadcrumb>
                <Card>
                    <div className="card-header">
                        <h2>Work Schedule</h2>
                    </div>
                    <div className="card-body">
                        <Form
                            noValidate
                            validated={validated}
                            onSubmit={this.handleSubmit}
                        >
                            <Form.Group as={Row} controlId="order_item_id">
                                <Form.Label column sm={1}>
                                    Order Item
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control as="select" value={workScheduleItem.order_item_id} onChange={this.handleChange}>
                                        {orders.map((order, index) => {
                                            return <option key={index} value={order.id}>{order.id + " " + order.product.name}</option>
                                        })}
                                    </Form.Control>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="estimated_completion_hours">
                                <Form.Label column sm={1}>
                                    Estimated completion hours
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="text" value={estimatedCompletionHours} readOnly/>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="planned_hours">
                                <Form.Label column sm={1}>
                                    Planned hours
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="text" value={plannedHours} readOnly/>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="employee_id">
                                <Form.Label column sm={1}>
                                    Employee
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control as="select" value={workScheduleItem.employee_id} onChange={this.handleChange}>
                                        {employees.map((employee, index) => {
                                            return <option key={index} value={employee.id}>{employee.id + " " + employee.name}</option>
                                        })}
                                    </Form.Control>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="available_work_hours">
                                <Form.Label column sm={1}>
                                    Available work hours
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="text" value={availableWorkHours} readOnly/>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="start">
                                <Form.Label column sm={1}>
                                    Start
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="time" value={workScheduleItem.start} onChange={this.handleChange} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="end">
                                <Form.Label column sm={1}>
                                    End
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="time" value={workScheduleItem.end} readOnly onChange={this.handleChange}/>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="date">
                                <Form.Label column sm={1}>
                                    Date
                                </Form.Label>
                                <Col sm={4}>
                                    <Form.Control type="date" value={workScheduleItem.date} onChange={this.handleChange} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="duration">
                                <Form.Label column sm={1}>
                                    Duration
                                </Form.Label>
                                <Col sm={4}>
                                    <div className='slider orientation-reversed'>
                                        <div className='slider-group'>
                                            <div className='slider-horizontal'>
                                                <Slider
                                                    min={0}
                                                    max={availableWorkHours}
                                                    value={horizontal}
                                                    orientation='horizontal'
                                                    onChange={this.handleChangeHorizontal}
                                                />
                                                <div className='value'>{horizontal}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            </Form.Group>
                            <Button variant="primary" type="submit">Create</Button>
                        </Form>
                    </div>
                </Card>
            </React.Fragment>
        );
    }

    createWorkScheduleItem = () => {
        const workScheduleItem = {...this.state.workScheduleItem};
        workScheduleItem.start = moment(workScheduleItem.date + " " + workScheduleItem.start).format("YYYY-MM-DD HH:mm:ss");
        workScheduleItem.end = moment(workScheduleItem.date + " " + workScheduleItem.end).format("YYYY-MM-DD HH:mm:ss");
        axios.post(UrlConstants.WORK_SCHEDULES, workScheduleItem)
            .then(res => {
                this.generateAlert("success", "Success", "Work schedule added!");
                //Wait 3 seconds before redirecting user
                setTimeout(function() {
                    window.location.href = "/capacityplan/";
                }, 3000);
            })
    }
    
    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }

        this.setState({alerts: [...this.state.alerts, newAlert]});
    }

    renderAlerts = () => {
        return (
            <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed} alerts={this.state.alerts} />
        );
    }
    
    getOrders = () => {
        axios.get(UrlConstants.PRODUCTION_ORDERS + "?includes=orderItemStatus,product&status_id[]=1&status_id[]=2&status_id[]=3")
            .then(res => {
                if(res.data[0] !== undefined) {
                    this.setState({
                        orders: res.data,
                        workScheduleItem: {...this.state.workScheduleItem, order_item_id: res.data[0].id}
                    }, () => this.getProductRemainingHours(this.state.workScheduleItem.order_item_id))
                } else{
                    this.setState({
                        orders: res.data,
                        workScheduleItem: {...this.state.workScheduleItem, order_item_id: res.data}
                    }, () => this.getProductRemainingHours(this.state.workScheduleItem.order_item_id))
                }
            });
    }

    getEmployees = () => {
        axios.get(UrlConstants.EMPLOYEES + "?")
            .then(res => {
                this.setState({employees: res.data, isLoading:false})
            });
    }
    
    getAvailableWorkHours = (employeeId, date) => {
        axios.get(UrlConstants.AVAILABLE_WORK_HOURS(employeeId, date))
            .then(res => this.setState({availableWorkHours: res.data.available_work_hours}));
    }
    
    getProductRemainingHours = ($id) => {
        axios.get(UrlConstants.REMAINING_PRODUCTION_HOURS($id))
            .then(res => this.setState({estimatedCompletionHours: res.data.estimated_completion_hours,
                plannedHours: res.data.total_hours_planned}))
    }
}

export default WorkScheduleForm;