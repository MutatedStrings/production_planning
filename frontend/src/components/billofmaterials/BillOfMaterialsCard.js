import React from 'react';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import StatusBadge from '../StatusBadge';
import BillItemRow from '../billitems/BillItemRow';
import BillOfMaterialsToolbar from './BillOfMaterialsToolbar';

const BillOfMaterialsCard = ({billOfMaterials}) => {
    return (
        <Card>
            <div className="card-header">
                <h2>Bill Of Materials #{billOfMaterials.id}</h2>
            </div>
            <div className="card-body">
                <BillOfMaterialsToolbar billOfMaterials={billOfMaterials} />
                <Table>
                    <tbody>
                        <tr>
                            <th scope="row">Status</th>
                            <td><StatusBadge status={billOfMaterials.bom_status.status} /></td>
                        </tr>
                    </tbody>
                </Table>
                <Table variant="bg-white">
                    <thead>
                        <tr>
                            <th scope="col">Bom Item</th>
                            <th scope="col">Material</th>
                            <th scope="col">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        {billOfMaterials.bill_items.map(billItem => <tr key={billItem.id}><BillItemRow billItem={billItem} /></tr>)}
                    </tbody>
                </Table>
            </div>
        </Card>
    );
}

export default BillOfMaterialsCard;