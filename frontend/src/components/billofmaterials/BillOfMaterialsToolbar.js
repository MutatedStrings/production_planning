import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import UrlConstants from '../../utils/UrlConstants';
import { AlertList } from 'react-bs-notifier';

class BillOfMaterialsToolbar extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            alerts: []
        }
    }
    
    onAlertDismissed = (alert) => {
        const alerts = this.state.alerts;
        
        const idx = alerts.indexOf(alert);
        if(idx >= 0) {
            this.setState({alerts: [...alerts.slice(0, idx), ...alerts.slice(idx+1)]});
        }
    }
    
    render() {
        var billOfMaterialStatus = this.props.billOfMaterials.bom_status.status;
        var alerts = this.renderAlerts();
        
        if (billOfMaterialStatus === 'Dispatched') {
            return (
                <React.Fragment>
                    {alerts}
                    <div>
                        <Button variant="info" onClick={this.updateBomStatus} >Order Items Received</Button>
                    </div>
                    <br />
                </React.Fragment>
            );
        } else {
            return (<div />);
        }
    }
    
    updateBomStatus = () => {
        var billOfMaterial = this.props.billOfMaterials;
        axios.put(UrlConstants.BILL_OF_MATERIAL(billOfMaterial.id), {
            status: "Received"
        })
        .then(response => {
            this.generateAlert("success", "Success", "Order items received");
            this.generateAlert("info", "Info", "Starting production");
            //Wait 3 seconds before reloading page
            setTimeout(function() {
                window.location.reload();
            }, 3000);
        })
        .catch(error => {
            console.error(error);
            if(error.response && error.response.status === 422) {
                var errors = error.response.data.error.message;
                Object.keys(errors).map((key, i) =>{
                    return this.generateAlert("danger", "Whoa, something's not right!", errors[key]);
                });
            }
            if(error.response && error.response.status === 500) {
                this.generateAlert("danger", "Whoa, something's not right!", "Server Error");
            }
        })
    }
    
    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }
        
        this.setState({alerts: [...this.state.alerts, newAlert]});
    }
    
    renderAlerts = () => {
        return (
            <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed} alerts={this.state.alerts} />
        );
    }
}

export default BillOfMaterialsToolbar;