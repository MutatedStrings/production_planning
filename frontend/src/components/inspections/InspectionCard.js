import React from 'react';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import StatusBadge from '../StatusBadge';

const InspectionCard = ({inspection}) => {
    return (
        <Card>
            <div className="card-header">
                <h2>Inspection #{inspection.id}</h2>
            </div>
            <div className="card-body">
                <Table>
                <tbody>
                    <tr>
                        <th scope="column">Number</th>
                        <td>{inspection.id}</td>
                    </tr>
                    <tr>
                        <th scope="column">Production Order</th>
                        <td>{inspection.order_item_id}</td>
                    </tr>
                    <tr>
                        <th scope="column">Due Date</th>
                        <td>{inspection.inspection_date}</td>
                    </tr>
                    <tr>
                        <th scope="column">Inspector</th>
                        <td>{inspection.inspector_id} {inspection.employee.name}</td>
                    </tr>
                    <tr>
                        <th scope="column">Status</th>
                        <td><StatusBadge status={inspection.inspection_result} /></td>
                    </tr>
                </tbody>
                </Table>
            </div>
        </Card>
    );
}

export default InspectionCard;