import React, { Component } from 'react';
import axios from 'axios';
import UrlConstants from '../../utils/UrlConstants';
import { ClipLoader } from 'react-spinners';
import Table from 'react-bootstrap/Table';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import ProductionOrderRow from '../productionorders/ProductionOrderRow';
import InspectionRow from './InspectionRow';


class InspectionLists extends Component {
    constructor() {
        super();
        
        this.state = {
            orders: [],
            inspections: [],
            isLoading: true,
        }
    }
        
    componentDidMount() {
        this.setState({isLoading:true});
        this.getOrders();
        this.getInspections();
    }
        
    renderOrders() {
        if(this.state.orders.length === 0 && !this.state.isLoading) {
            return (<tr><td> No pending inspections. </td></tr>);
        }
        return this.state.orders.map(order => {
            return (
                <tr key={order.id}>
                    <ProductionOrderRow order={order} />
                </tr>
            );
        })
    }
    
    renderInspections() {
        if(this.state.inspections.length === 0 && !this.state.isLoading) {
            return (<tr><td> No inspections completed yet. </td></tr>);
        }
        return this.state.inspections.map(inspection => {
            return (
                <tr key={inspection.id}>
                    <InspectionRow inspection={inspection} />
                </tr>
            )
        })
    }
        
    render() {
        if(this.state.isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>
            )
        }
            
        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item active>Inspections</Breadcrumb.Item>
                </Breadcrumb>
                <h1>Pending Inspections</h1>
                <Table hover variant="bg-white">
                    <thead>
                        <tr>
                            <th scope="col">Order No</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Product ID</th>
                            <th scope="col">Status</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderOrders()}
                    </tbody>
                </Table>
                <h1>Completed Inpections</h1>
                <Table hover variant="bg-white">
                    <thead>
                        <tr>
                            <th scope="col">Inspection No</th>
                            <th scope="col">Date of Inspection</th>
                            <th scope="col">Inspector</th>
                            <th scope="col">Order Item</th>
                            <th scope="col">Inspection Result</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderInspections()}
                    </tbody>
                </Table>
            </div>
        )
    }
        
    getOrders = () => {
        axios.get(UrlConstants.PRODUCTION_ORDERS + "?includes=product,order.orderStatus,orderItemStatus&status=Awaiting Inspection")
            .then(res => this.setState({orders: res.data, isLoading:false}));
    }

    getInspections = () => {
        axios.get(UrlConstants.INSPECTIONS)
            .then(res => this.setState({inspections: res.data, isLoading:false}));
    }
}

export default InspectionLists;