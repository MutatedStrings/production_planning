import React from 'react';
import StatusBadge from '../StatusBadge';

const InspectionRow = ({inspection}) => {
    let button;
    
    if(inspection.inspection_result === "Rejected") {
        button = <a className="btn btn-primary" href={"/inspections/" + inspection.id + "/rejectionnotice"}>View Rejection Notice</a>;
    }
    return (
        <React.Fragment>
            <td>{inspection.id}</td>
            <td>{inspection.inspection_date}</td>
            <td>{inspection.inspector_id} {inspection.employee.name}</td>
            <td>{inspection.order_item_id}</td>
            <td><StatusBadge status={inspection.inspection_result} /></td>
            <td className="text-center">{button}</td>
        </React.Fragment>
    )
}

export default InspectionRow;