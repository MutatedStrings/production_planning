import React, {Component} from 'react';
import {DragDropContext} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import axios from 'axios';
import moment from 'moment';
import 'react-big-scheduler/lib/css/style.css'
import Scheduler, {SchedulerData, ViewTypes, DATE_FORMAT} from 'react-big-scheduler';
import {ClipLoader} from 'react-spinners';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import UrlConstants from '../../utils/UrlConstants';
import Alert from 'react-bootstrap/Alert';

class CapacityPlan extends Component {
    schedulerData;
    resources = [];
    events;

    constructor() {
        super();

        this.schedulerData = new SchedulerData(new moment().format(DATE_FORMAT), ViewTypes.Month);
        moment.locale();
        this.schedulerData.setLocaleMoment(moment);

        this.setResourcesAndEvents();

        this.state = {
            viewModel: this.schedulerData,
            events: this.events,
            employees: []
        }
    }

    componentDidMount() {
        this.getEmployees();
        this.getWorkSchedules();
    }

    setResourcesAndEvents = () => {
        this.schedulerData.setResources(this.resources);

        //set events here or later,
        //the event array should be sorted in ascending order by event.start property, otherwise there will be some rendering errors
        this.events = [];
        this.schedulerData.setEvents(this.events);
    }

    render() {
        const {viewModel} = this.state;

        return (
            <>
                <Scheduler schedulerData={viewModel}
                           prevClick={this.prevClick}
                           nextClick={this.nextClick}
                           onSelectDate={this.onSelectDate}
                           onViewChange={this.onViewChange}
                           eventItemClick={this.eventClicked}
                           newEvent={this.newEvent}
                />
            </>
        );
    }

    prevClick = (schedulerData) => {
        schedulerData.prev();
        schedulerData.setEvents(this.state.events);
        this.setState({
            viewModel: schedulerData
        })
    }

    nextClick = (schedulerData) => {
        schedulerData.next();
        schedulerData.setEvents(this.state.events);
        this.setState({
            viewModel: schedulerData
        })
    }

    onViewChange = (schedulerData, view) => {
        schedulerData.setViewType(view.viewType, view.showAgenda, view.isEventPerspective);
        schedulerData.setEvents(this.state.events);
        this.setState({
            viewModel: schedulerData
        })
    }

    onSelectDate = (schedulerData, date) => {
        schedulerData.setDate(date);
        schedulerData.setEvents(this.state.events);
        this.setState({
            viewModel: schedulerData
        })
    }

    eventClicked = (schedulerData, event) => {
        alert(`You just clicked an event: {id: ${event.id}, title: ${event.title}}`);
    };

    newEvent = (schedulerData, slotId, slotName, start, end, type, item) => {
        if (window.confirm(`Do you want to create a new event?`)) {
            this.props.history.push({
                pathname: "/capacityplan/workschedule",
                state: {start: start, end: end, employeeId: slotId}
            });
        }
    }

    getEmployees = () => {
        axios.get(UrlConstants.EMPLOYEES + "?employee_type=PO")
            .then(res => {
                this.schedulerData.setResources(res.data);
                this.setState({employees: res.data, viewModel: this.schedulerData})
            });
    }

    getWorkSchedules = () => {
        axios.get(UrlConstants.WORK_SCHEDULES + "?includes=orderItem.product")
            .then(res => {
                let event;
                res.data.map((scheduleItem, index) => {
                    event = {};
                    event.id = scheduleItem.id;
                    event.start = scheduleItem.start;
                    event.end = scheduleItem.end;
                    event.resourceId = scheduleItem.employee_id;
                    event.title = scheduleItem.order_item_id + " " + scheduleItem.order_item.product.name;
                    this.events.push(event);
                });
                this.schedulerData.setEvents(this.events);
                this.setState({vuewModel: this.schedulerData});
            })
    }

}

export default DragDropContext(HTML5Backend)(CapacityPlan);