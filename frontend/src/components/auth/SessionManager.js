import {EMAIL, NAME, USER_ROLE, REFRESH_TOKEN, TOKEN, USER_ID} from "../../utils/Constants";

export const setSessionValue = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
};

export const getSessionValue = (key) => {
    let value = localStorage.getItem(key);
    if (value === '') {
        return value
    }
    return JSON.parse(localStorage.getItem(key));
};

export const getAuthToken = () => {
    let token = localStorage.getItem(TOKEN);
    if (!token) {
        return '';
    }
    return JSON.parse(token);
};

export const getRole = () => {
    let role = localStorage.getItem(USER_ROLE);
    if (!role) {
        return '';
    }
    return JSON.parse(role);
};

export const deleteSession = () => {
    localStorage.setItem(TOKEN, '');
    localStorage.setItem(USER_ID, '');
    localStorage.setItem(USER_ROLE, '');
    localStorage.setItem(EMAIL, '');
    localStorage.setItem(NAME, '');
    localStorage.setItem(REFRESH_TOKEN, '');
};