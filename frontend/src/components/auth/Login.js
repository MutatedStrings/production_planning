import React, {Component} from 'react';
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import axios from "axios";
import {getAuthToken, getSessionValue, setSessionValue} from "./SessionManager";
import {
    EMAIL,
    PASSWORD_GRANT_CLIENT_ID,
    PASSWORD_GRANT_CLIENT_SECRET,
    PASSWORD_GRANT_TYPE,
    REFRESH_TOKEN,
    TOKEN, USER_ID,
    USER_ROLE,
    NAME
} from "../../utils/Constants";
import UrlConstants from "../../utils/UrlConstants";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {AlertList} from "react-bs-notifier";

class Login extends Component {
    state = {
        tokenRequest: {},
        alerts: []
    };

    handleChange = (event) => {
        const {id, value} = event.target;
        this.setState({tokenRequest: {...this.state.tokenRequest, [id] : value}});
    };

    componentDidMount() {
        if(getAuthToken() !== '') {
            this.props.history.push("/");
        }
    }


    render() {
        let alerts = this.renderAlerts();
        return (
            <Container>
                {alerts}
                <h1>Sign in</h1><br/>
                <Form onSubmit={this.handleLogin}>
                    <Form.Group as={Row} controlId="username">
                        <Form.Label column sm={1}>
                            Email
                        </Form.Label>
                        <Col>
                            <Form.Control type="email" required onChange={this.handleChange}/>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="password">
                        <Form.Label column sm={1}>
                            Password
                        </Form.Label>
                        <Col>
                            <Form.Control type="password" required onChange={this.handleChange}/>
                        </Col>
                    </Form.Group>
                    <Button type="submit" variant="primary">Sign in</Button>
                </Form>
            </Container>
        );
    }

    handleLogin = (event) => {
        event.preventDefault();
        let {tokenRequest} = this.state;
        tokenRequest.grant_type = PASSWORD_GRANT_TYPE;
        tokenRequest.scope = "*";
        tokenRequest.client_id = PASSWORD_GRANT_CLIENT_ID;
        tokenRequest.client_secret = PASSWORD_GRANT_CLIENT_SECRET;
        axios.post(UrlConstants.OAUTH_TOKEN_ROUTE, tokenRequest)
            .then(res => {
                setSessionValue(TOKEN, res.data.access_token);
                setSessionValue(REFRESH_TOKEN, res.data.refresh_token);
                setSessionValue(EMAIL, tokenRequest.username);
                axios.defaults.headers.common = {'Authorization': `Bearer ${res.data.access_token}`};
                this.getUser();
                this.props.history.push("/")
            })
            .catch(error => {
                if(error.response !== undefined) {
                    if (error.response.status !== undefined && (error.response.status === 400 || error.response.status === 401)) {
                        this.generateAlert("danger", "Whoa, something's not right!", error.response.data.message);
                    } else {
                        this.generateAlert("danger", "Whoa something's not right!", error);
                    }
                }
            });
    };

    getUser = () => {
        axios.get(UrlConstants.EMPLOYEES + "?email=" + getSessionValue(EMAIL))
            .then(res => {
                setSessionValue(USER_ROLE, res.data[0].employee_type);
                setSessionValue(USER_ID, res.data[0].id);
                setSessionValue(NAME, res.data[0].name);
            })
    }

    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }

        this.setState({alerts: [...this.state.alerts, newAlert]});
    }

    renderAlerts = () => {
        return (
            <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed}
                       alerts={this.state.alerts}/>
        );
    }
}

export default Login;
