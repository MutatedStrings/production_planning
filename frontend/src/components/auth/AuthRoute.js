import React from 'react';
import {getAuthToken, getRole} from "./SessionManager";
import {Redirect, Route} from "react-router-dom";
import Alert from "react-bootstrap/Alert";

const checkAuth = () => {
    return getAuthToken();
}

export const AuthRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        checkAuth() ? (
            <Component {...props} />
        ) : (
            <Redirect to={{ pathname: '/login' }} />
        )
    )} />
);

export const Authorization = (allowedRoles) => (WrappedComponent) => {
    return class WithAuthorization extends React.Component {
        render() {
            if (allowedRoles.some(role => role === getRole())) {
                return <WrappedComponent {...this.props} />
            } else {
                return (<Alert variant={"warning"}>"You are not authorized to view this page"</Alert>);
            }
        }
    };
};

export const ADMIN = Authorization(['ADMIN']);
export const PRODUCTION_MANAGER = Authorization(['ADMIN', 'PM']);
export const PRODUCTION_OPERATIVE = Authorization(['ADMIN', 'PM', 'PO']);
export const PRODUCTION_INSPECTOR = Authorization(['ADMIN', 'PM', 'PI']);