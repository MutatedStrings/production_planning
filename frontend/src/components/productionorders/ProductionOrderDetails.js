import React, { Component } from 'react';
import axios from 'axios';
import { ClipLoader } from 'react-spinners';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import UrlConstants from '../../utils/UrlConstants';
import ProductionOrderCard from './ProductionOrderCard';
import Alert from 'react-bootstrap/Alert';
import BillOfMaterialsCard from '../billofmaterials/BillOfMaterialsCard';

class ProductionOrderDetails extends Component {
    constructor() {
        super();
        
        this.state = {
            productionOrder: {},
            billOfMaterial: {},
            isLoading: true,
            errors: []
        }
    }
    
    componentDidMount() {
        this.getOrder();
        this.getBillOfMaterial();
    }
    
    render() {
        if(this.state.isLoading) {
            return (
                <div className="App-header">
                    <ClipLoader 
                        sizeUnit={"px"}
                        size={80}
                        color={"#4A90E2"}
                        loading={this.state.isLoading}
                    />
                </div>);
        }
        
        if(this.state.errors.length !== 0) {
            return (<React.Fragment>{this.renderErrors()}</React.Fragment>);
        }
        
        return (
            <React.Fragment>
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
                    <Breadcrumb.Item href="/productionorders">Production Orders</Breadcrumb.Item>
                    <Breadcrumb.Item active>{this.state.productionOrder.id}</Breadcrumb.Item>
                </Breadcrumb>
                <ProductionOrderCard productionOrder={this.state.productionOrder} />
                {this.state.billOfMaterial !== undefined ? (<BillOfMaterialsCard billOfMaterials={this.state.billOfMaterial} />) : <></>}
            </React.Fragment>
        );
    }
    
    getOrder = () => {
        let orderId = this.props.match.params.productionOrderId;
        axios.get(UrlConstants.PRODUCTION_ORDERS + "/" + orderId)
            .then(res => this.setState({productionOrder: res.data}))
            .catch(error => {
                console.error(error);
                if(error.response.status === 404) {
                    var message = 'No such production order found!';
                    this.setState({errors: [...this.state.errors, message], isLoading:false});
                }
            })
    }
    
    getBillOfMaterial = () => {
        let orderId = this.props.match.params.productionOrderId;
        axios.get(UrlConstants.BILL_OF_MATERIALS + "?order_item_id=" + orderId + "&includes=billItems,bomStatus")
            .then(res=> this.setState({billOfMaterial: res.data[0], isLoading:false}));
    }
    
    renderErrors = () => {
        const { errors } = this.state;
        return Object.keys(errors).map((key, i) => {
            return (
                <Alert key={key} variant="danger">
                    <Alert.Heading>Something's not right</Alert.Heading>
                    <p>
                        {errors[key]}
                    </p>
                </Alert>
            )
        });
    }
}

export default ProductionOrderDetails;