import React from 'react';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import StatusBadge from '../StatusBadge';
import ProductionOrderToolbar from './ProductionOrderToolbar';

const ProductionOrderCard = ({productionOrder}) => {
    return (
        <Card>
            <div className="card-header">
                <h2>Production Order #{productionOrder.id}</h2>
            </div>
            <div className="card-body">
                <ProductionOrderToolbar productionOrder={productionOrder} />
                <Table>
                <tbody>
                    <tr>
                        <th scope="column">Number</th>
                        <td>{productionOrder.id}</td>
                    </tr>
                    <tr>
                        <th scope="column">Product ID</th>
                        <td>{productionOrder.product_id} {productionOrder.product.name}</td>
                    </tr>
                    <tr>
                        <th scope="column">Quantity</th>
                        <td>{productionOrder.quantity}</td>
                    </tr>
                    <tr>
                        <th scope="column">Status</th>
                        <td><StatusBadge status={productionOrder.order_item_status.status} /></td>
                    </tr>
                    <tr>
                        <th scope="column">Sales Order</th>
                        <td>{productionOrder.order.id} <StatusBadge status={productionOrder.order.order_status.status} /></td>
                    </tr>
                </tbody>
                </Table>
            </div>
        </Card>
    );
}

export default ProductionOrderCard;