import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import moment from 'moment/moment';
import UrlConstants from '../../utils/UrlConstants';
import { AlertList } from 'react-bs-notifier';
import {getRole, getSessionValue} from "../auth/SessionManager";
import {USER_ID} from "../../utils/Constants";

class ProductionOrderToolbar extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            alerts: []
        }
    }
    
    onAlertDismissed = (alert) => {
        const alerts = this.state.alerts;
        
        const idx = alerts.indexOf(alert);
        if(idx >= 0) {
            this.setState({alerts: [...alerts.slice(0, idx), ...alerts.slice(idx+1)]});
        }
    }
    
    render() {
        var productionOrderStatus = this.props.productionOrder.order_item_status.status;
        var alerts = this.renderAlerts();
        // Inspection Toolbar
        if (productionOrderStatus === 'Awaiting Inspection' && ['ADMIN', 'PI'].some(role => role === getRole())) {
            return (
                <React.Fragment>
                    {alerts}
                    <div>
                        <Button variant="success" onClick={this.approveOrder} href="#">Approve</Button> <Button variant="danger" href={"/productionorders/"+ this.props.productionOrder.id + "/reject"}>Reject</Button>
                    </div>
                    <br />
                </React.Fragment>
            );
        } 
        // Finish Production Toolbar
        else if (productionOrderStatus === 'In Progress' && ['ADMIN', 'PM', 'PO'].some(role => role === getRole())) {
            return (
                <React.Fragment>
                    {alerts}
                    <div>
                        <Button variant="success" onClick={this.finishProduction} href="#">Finish Production</Button>
                    </div>
                    <br />
                </React.Fragment>
            );
        } 
        else {
            return (<div />);
        }
    }
    
    approveOrder = () => {
        axios.post(UrlConstants.INSPECTIONS, {
            inspector_id: getSessionValue(USER_ID),
            inspection_result: "Approved",
            order_item_id: this.props.productionOrder.id,
            inspection_date: moment().format('YYYY-MM-DD')
        })
        .then(response => {
            this.generateAlert("success", "Success", "Production order item approved!");
            //Wait 3 seconds before reloading page
            setTimeout(function() {
                window.location.reload();
            }, 3000);
        })
        .catch(error => {
            console.error(error);
            if(error.response && error.response.status === 422) {
                var errors = error.response.data.error.message;
                Object.keys(errors).map((key, i) =>{
                    return this.generateAlert("danger", "Whoa, something's not right!", errors[key]);
                });
            }
            if(error.response && error.response.status === 500) {
                this.generateAlert("danger", "Whoa, something's not right!", "Server Error");
            }
        });
                
    }
            
    finishProduction = () => {
        var productionOrder = this.props.productionOrder;
        axios.put(UrlConstants.PRODUCTION_ORDER(productionOrder.id), {
            status: "Awaiting Inspection"
        })
        .then(response => {
            this.generateAlert("success", "Success", "Production order status updated!");
            //Wait 3 seconds before reloading page
            setTimeout(function() {
                window.location.reload();
            }, 3000);
        })
        .catch(error => {
            if(error.response && error.response.status === 422) {
                var errors = error.response.data.error.message;
                Object.keys(errors).map((key, i) =>{
                    return this.generateAlert("danger", "Whoa, something's not right!", errors[key]);
                });
            }
            if(error.response && error.response.status === 500) {
                this.generateAlert("danger", "Whoa, something's not right!", "Server Error");
            }
        })
    }
    
    generateAlert = (type, headline, message) => {
        const newAlert = {
            id: (new Date()).getTime(),
            type: type,
            headline: headline,
            message: message
        }

        this.setState({alerts: [...this.state.alerts, newAlert]});
    }

    renderAlerts = () => {
        return (
            <AlertList timeout={2000} position="bottom-right" onDismiss={this.onAlertDismissed} alerts={this.state.alerts} />
        );
    }
}

export default ProductionOrderToolbar;