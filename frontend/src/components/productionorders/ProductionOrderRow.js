import React from 'react';
import StatusBadge from '../StatusBadge';

const ProductionOrderRow = ({order}) => {
    return (
        <React.Fragment>
            <th scope="row">{order.id}</th>
            <td>{order.quantity}</td>
            <td>{order.product_id}</td>
            <td><StatusBadge status={order.order_item_status.status} /></td>
            <td className="text-center"><a className="btn btn-primary" href={"/productionorders/" + order.id}>View Order</a></td>
        </React.Fragment>
    );
}

export default ProductionOrderRow;