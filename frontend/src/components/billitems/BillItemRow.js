import React from 'react';

const BillItemRow = ({billItem}) => {
    return (
        <React.Fragment>
            <td>{billItem.id}</td>
            <td>{billItem.material_id}</td>
            <td>{billItem.quantity}</td>
        </React.Fragment>
    );
}

export default BillItemRow;