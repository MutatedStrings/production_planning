import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import Nav from 'react-bootstrap/Nav';

const NavDrawer = props => {
  return (
      <Menu customBurgerIcon = {false} customCrossIcon = {false} isOpen = {props.show} onStateChange={props.app} >
        <Nav.Link href="/">Home</Nav.Link>
        <Nav.Link href="/salesorders">Sales Orders</Nav.Link>
        <Nav.Link href="/productionorders">Production Orders</Nav.Link>
        <Nav.Link href="/capacityplan">Capacity Plan</Nav.Link>
        <Nav.Link href="/inspections">Inspections</Nav.Link>
      </Menu>
    );
}

export default NavDrawer;