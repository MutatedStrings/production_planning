<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('orders', 'OrdersController@index');

$router->group(['middleware' => 'client'], function () use ($router) {
    $router->group(['prefix' => 'api'], function () use ($router) {
        //Bill Items
        $router->get('billitems', 'BillItemsController@index');
        $router->post('billitems', 'BillItemsController@store');
        $router->get('billitems/{id}', 'BillItemsController@show');
        $router->put('billitems/{id}', 'BillItemsController@update');
        $router->delete('billitems/{id}', 'BillItemsController@destroy');

        //Bill Of Materials
        $router->get('billofmaterials', 'BillOfMaterialsController@index');
        $router->post('billofmaterials', 'BillOfMaterialsController@store');
        $router->get('billofmaterials/{id}', 'BillOfMaterialsController@show');
        $router->put('billofmaterials/{id}', 'BillOfMaterialsController@update');
        $router->delete('billofmaterials/{id}', 'BillOfMaterialsController@destroy');

        //Bom Statuses
        $router->get('bomstatuses', 'BomStatusesController@index');
        $router->post('bomstatuses', 'BomStatusesController@store');
        $router->get('bomstatuses/{id}', 'BomStatusesController@show');
        $router->put('bomstatuses/{id}', 'BomStatusesController@update');
        $router->delete('bomstatuses/{id}', 'BomStatusesController@destroy');

        //Employees
        $router->get('employees', 'EmployeesController@index');
        $router->post('employees', 'EmployeesController@store');
        $router->get('employees/{id}', 'EmployeesController@show');
        $router->put('employees/{id}', 'EmployeesController@update');
        $router->delete('employees/{id}', 'EmployeesController@destroy');
        $router->get('employees/{id}/availableworkhours/{date}', 'EmployeesController@availableWorkHours');

        //Inspections
        $router->get('inspections', 'InspectionsController@index');
        $router->post('inspections', 'InspectionsController@store');
        $router->get('inspections/{id}', 'InspectionsController@show');
        $router->put('inspections/{id}', 'InspectionsController@update');
        $router->delete('inspections/{id}', 'InspectionsController@destroy');

        //Leaves
        $router->get('leaves', 'LeavesController@index');
        $router->post('leaves', 'LeavesController@store');
        $router->get('leaves/{id}', 'LeavesController@show');
        $router->put('leaves/{id}', 'LeavesController@update');
        $router->delete('leaves/{id}', 'LeavesController@destroy');

        //LeaveTypes
        $router->get('leavetypes', 'LeaveTypesController@index');
        $router->post('leavetypes', 'LeaveTypesController@store');
        $router->get('leavetypes/{id}', 'LeaveTypesController@show');
        $router->put('leavetypes/{id}', 'LeaveTypesController@update');
        $router->delete('leavetypes/{id}', 'LeaveTypesController@destroy');

        //Materials
        $router->get('materials', 'MaterialsController@index');
        $router->post('materials', 'MaterialsController@store');
        $router->get('materials/{id}', 'MaterialsController@show');
        $router->put('materials/{id}', 'MaterialsController@update');
        $router->delete('materials/{id}', 'MaterialsController@destroy');

        //Orders
        $router->get('orders', 'OrdersController@index');
        $router->post('orders', 'OrdersController@store');
        $router->get('orders/{id}', 'OrdersController@show');
        $router->put('orders/{id}', 'OrdersController@update');
        $router->delete('orders/{id}', 'OrdersController@destroy');

        //Order Items
        $router->get('orderitems', 'OrderItemsController@index');
        $router->post('orderitems', 'OrderItemsController@store');
        $router->get('orderitems/{id}', 'OrderItemsController@show');
        $router->put('orderitems/{id}', 'OrderItemsController@update');
        $router->delete('orderitems/{id}', 'OrderItemsController@destroy');
        $router->get('orderitems/{id}/remaininghours', 'OrderItemsController@remainingHours');

        //Order Item Statuses
        $router->get('orderitemstatuses', 'OrderItemStatusesController@index');
        $router->post('orderitemstatuses', 'OrderItemStatusesController@store');
        $router->get('orderitemstatuses/{id}', 'OrderItemStatusesController@show');
        $router->put('orderitemstatuses/{id}', 'OrderItemStatusesController@update');
        $router->delete('orderitemstatuses/{id}', 'OrderItemStatusesController@destroy');

        //Order Statuses
        $router->get('orderstatuses', 'OrderStatusesController@index');
        $router->post('orderstatuses', 'OrderStatusesController@store');
        $router->get('orderstatuses/{id}', 'OrderStatusesController@show');
        $router->put('orderstatuses/{id}', 'OrderStatusesController@update');
        $router->delete('orderstatuses/{id}', 'OrderStatusesController@destroy');

        //Products
        $router->get('products', 'ProductsController@index');
        $router->post('products', 'ProductsController@store');
        $router->get('products/{id}', 'ProductsController@show');
        $router->put('products/{id}', 'ProductsController@update');
        $router->delete('products/{id}', 'ProductsController@destroy');

        //Product Structures
        $router->get('productstructures', 'ProductStructuresController@index');
        $router->post('productstructures', 'ProductStructuresController@store');
        $router->get('productstructures/{id}', 'ProductStructuresController@show');
        $router->put('productstructures/{id}', 'ProductStructuresController@update');
        $router->delete('productstructures/{id}', 'ProductStructuresController@destroy');

        //Product Structure Items
        $router->get('productstructureitems', 'ProductStructureItemsController@index');
        $router->post('productstructureitems', 'ProductStructureItemsController@store');
        $router->get('productstructureitems/{id}', 'ProductStructureItemsController@show');
        $router->put('productstructureitems/{id}', 'ProductStructureItemsController@update');
        $router->delete('productstructureitems/{id}', 'ProductStructureItemsController@destroy');

        //Rejection Notices
        $router->get('rejectionnotices', 'RejectionNoticesController@index');
        $router->post('rejectionnotices', 'RejectionNoticesController@store');
        $router->get('rejectionnotices/{id}', 'RejectionNoticesController@show');
        $router->put('rejectionnotices/{id}', 'RejectionNoticesController@update');
        $router->delete('rejectionnotices/{id}', 'RejectionNoticesController@destroy');

        //Work Schedule Items
        $router->get('workscheduleitems', 'WorkScheduleItemsController@index');
        $router->post('workscheduleitems', 'WorkScheduleItemsController@store');
        $router->get('workscheduleitems/{id}', 'WorkScheduleItemsController@show');
        $router->put('workscheduleitems/{id}', 'WorkScheduleItemsController@update');
        $router->delete('workscheduleitems/{id}', 'WorkScheduleItemsController@destroy');
    });
});
