<?php

namespace App\CustomFilters;

use Unlu\Laravel\Api\QueryBuilder;

class OrderItemQueryBuilder extends QueryBuilder {
    public function filterByStatus($query, $status) {
        return $query->whereHas('orderItemStatus', function($q) use ($status) {
                   $q->where('status', $status);
                });
    }
}
