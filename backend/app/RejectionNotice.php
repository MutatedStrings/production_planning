<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $inspection_id
 * @property string $rejection_cause
 * @property string $action
 * @property string $created_at
 * @property string $updated_at
 * @property Inspection $inspection
 */
class RejectionNotice extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new rejection notice object is being created
        static::creating(function ($rejectionNotice) {
            $lastRejectionNotice = RejectionNotice::orderBy('id', 'desc')->take(1)->first();
            $lastRejectionNoticeId = null;
            $newRejectionNoticeId = null;
            if ($lastRejectionNotice) {
                $lastRejectionNoticeId = $lastRejectionNotice->id;
                $newRejectionNoticeId = ++$lastRejectionNoticeId;
            } else {
                $newRejectionNoticeId = 'RN0001';
            }
            $rejectionNotice->{$rejectionNotice->getKeyName()} = $newRejectionNoticeId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'rejection_notice';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['inspection_id', 'rejection_cause', 'action', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inspection()
    {
        return $this->belongsTo('App\Inspection');
    }
}
