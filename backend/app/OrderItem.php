<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $product_id
 * @property string $order_id
 * @property int $status_id
 * @property int $quantity
 * @property string $completion_date
 * @property int $order_item_ref_id
 * @property string $created_at
 * @property string $updated_at
 * @property Product $product
 * @property Order $order
 * @property OrderItemStatus $orderItemStatus
 * @property BillOfMaterial[] $billOfMaterials
 * @property Inspection[] $inspections
 * @property RejectionNotice[] $rejectionNotices
 * @property WorkScheduleItem[] $workScheduleItems
 */
class OrderItem extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new order item object is being created
        static::creating(function ($orderItem) {
            $lastOrderItem = OrderItem::orderBy('id', 'desc')->take(1)->first();
            $lastOrderItemId = null;
            $newOrderItemId = null;
            if ($lastOrderItem) {
                $lastOrderItemId = $lastOrderItem->id;
                $newOrderItemId = ++$lastOrderItemId;
            } else {
                $newOrderItemId = 'PO0001';
            }
            $orderItem->{$orderItem->getKeyName()} = $newOrderItemId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'order_item';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'order_id', 'status_id', 'quantity', 'completion_date', 'order_item_ref_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderItemStatus()
    {
        return $this->belongsTo('App\OrderItemStatus', 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function billOfMaterials()
    {
        return $this->hasOne('App\BillOfMaterial');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function inspections()
    {
        return $this->hasOne('App\Inspection');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workScheduleItems()
    {
        return $this->hasMany('App\WorkScheduleItem');
    }
}
