<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $bom_id
 * @property string $material_id
 * @property int $quantity
 * @property string $created_at
 * @property string $updated_at
 * @property Material $material
 * @property BillOfMaterial $billOfMaterial
 */
class BillItem extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new bill item object is being created
        static::creating(function ($billItem) {
            $lastBillItem = BillItem::orderBy('id', 'desc')->take(1)->first();
            $billItemId = null;
            $newBillItemId = null;
            if ($lastBillItem) {
                $billItemId = $lastBillItem->id;
                $newBillItemId = ++$billItemId;
            } else {
                $newBillItemId = 'BI0001';
            }
            $billItem->{$billItem->getKeyName()} = $newBillItemId;
        });
    }    
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'bill_item';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['bom_id', 'material_id', 'quantity', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function material()
    {
        return $this->belongsTo('App\Material');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function billOfMaterial()
    {
        return $this->belongsTo('App\BillOfMaterial', 'bom_id');
    }
}
