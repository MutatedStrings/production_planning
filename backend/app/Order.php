<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property int $status_id
 * @property string $due_date
 * @property string $projected_completion_date
 * @property int $order_ref_id
 * @property string $created_at
 * @property string $updated_at
 * @property OrderStatus $orderStatus
 * @property OrderItem[] $orderItems
 */
class Order extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new employee object is being created
        static::creating(function ($order) {
            $lastOrder = Order::orderBy('id', 'desc')->take(1)->first();
            $lastOrderId = null;
            $newOrderId = null;
            if ($lastOrder) {
                $lastOrderId = $lastOrder->id;
                $newOrderId = ++$lastOrderId;
            } else {
                $newOrderId = 'C00001';
            }
            $order->{$order->getKeyName()} = $newOrderId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'order';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['status_id', 'due_date', 'order_ref_id', 'projected_completion_date', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderStatus()
    {
        return $this->belongsTo('App\OrderStatus', 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }
}
