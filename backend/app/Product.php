<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $name
 * @property int $product_ref_id
 * @property string $created_at
 * @property string $updated_at
 * @property OrderItem[] $orderItems
 * @property ProductStructure $productStructure
 */
class Product extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new product object is being created
        static::creating(function ($product) {
            $lastProduct = Product::orderBy('id', 'desc')->take(1)->first();
            $lastProductId = null;
            $newProductId = null;
            if ($lastProduct) {
                $lastProductId = $lastProduct->id;
                $newProductId = ++$lastProductId;
            } else {
                $newProductId = 'P00001';
            }
            $product->{$product->getKeyName()} = $newProductId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'product';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['name', 'product_ref_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function productStructure()
    {
        return $this->hasOne('App\ProductStructure');
    }
}
