<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $product_id
 * @property float $estimated_completion_hours
 * @property string $created_at
 * @property string $updated_at
 * @property Product $product
 * @property ProductStructureItem[] $productStructureItems
 */
class ProductStructure extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new product structure object is being created
        static::creating(function ($productStructure) {
            $lastProductStructure = ProductStructure::orderBy('id', 'desc')->take(1)->first();
            $lastProductStructureId = null;
            $newProductStructureId = null;
            if ($lastProductStructure) {
                $lastProductStructureId = $lastProductStructure->id;
                $newProductStructureId = ++$lastProductStructureId;
            } else {
                $newProductStructureId = 'PS0001';
            }
            $productStructure->{$productStructure->getKeyName()} = $newProductStructureId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'product_structure';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['product_id', 'estimated_completion_hours', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productStructureItems()
    {
        return $this->hasMany('App\ProductStructureItem', 'structure_id');
    }
}
