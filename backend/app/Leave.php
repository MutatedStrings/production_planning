<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $code
 * @property string $employee_id
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 * @property Employee $employee
 * @property LeaveType $leaveType
 */
class Leave extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'leave';

    /**
     * @var array
     */
    protected $fillable = ['code', 'employee_id', 'date', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaveType()
    {
        return $this->belongsTo('App\LeaveType', 'code', 'leave_code');
    }
}
