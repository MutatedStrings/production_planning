<?php

namespace App\Services;

use App\Inspection;
use Illuminate\Http\Request;
use Validator;
use App\Services\OrderItemService;

class InspectionService {
    private $orderItemService;
    
    public function __construct(OrderItemService $orderItemService) {
        $this->orderItemService = $orderItemService;
    }
    
    public function getAll() {
        return Inspection::with('employee', 'orderItem')->get();
    }
    
    public function get($id) {
        return Inspection::with('employee', 'orderItem')->findOrFail($id);
    }
    
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), $this->postRequestValidationRules());
        $validator->validate();
        $inspection = Inspection::create([
            'inspector_id' => $request->get('inspector_id'),
            'order_item_id' => $request->get('order_item_id'),
            'inspection_date' => $request->get('inspection_date'),
            'inspection_result' => $request->get('inspection_result')
        ]);
        
        //Update Order Item status to Complete once inspection is approved
        $orderItemId = $inspection->order_item_id;
        if($request->get('inspection_result') == "Approved") {
            $this->orderItemService->updateOrderItemStatus($orderItemId, "Complete");
        }
        
        return $inspection;
    }
    
    public function update(Request $request, $id) {
        $inspection = Inspection::find($id);
        $inspection->update($inspection, $request->all());
        return $inspection;
    }
    
    /**
     * Post Request Validation Rules
     *
     * @param Request $request
     * @return array
     */
    private function postRequestValidationRules()
    {
        $rules = [
            'inspector_id'      => 'required',
            'order_item_id'     => 'required',
            'inspection_date'   => 'required',
            'inspection_result' => 'required'
        ];
        //TODO 
//        $requestUser = $request->user();
//        // Only admin user can set admin role.
//        if ($requestUser instanceof User && $requestUser->role === User::ADMIN_ROLE) {
//            $rules['role'] = 'in:BASIC_USER,ADMIN_USER';
//        } else {
//            $rules['role'] = 'in:BASIC_USER';
//        }
        return $rules;
    }
}
