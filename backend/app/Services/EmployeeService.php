<?php

namespace App\Services;

use App\Employee;
use App\WorkScheduleItem;
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;
use Validator;

class EmployeeService {
    public function getAll(Request $request) {
        $queryBuilder = new QueryBuilder(new Employee, $request);
        return $queryBuilder->build()->get();
    }
    
    public function get($id) {
        return Employee::findOrFail($id);
    }
    
    public function create(array $request) {
        $validator = Validator::make($request, $this->postRequestValidationRules());
        $validator->validate();
        $employee =  Employee::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'contact_number' => $request['contact_number'],
            'employee_type' => $request['employee_type'],
        ]);
        
        return $employee;
    }

    /**
     * Gets the remaining work hours for a day for a certain employee
     * @param $id
     * @param $date
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableWorkHours($id, $date) {
        $employee = Employee::findOrFail($id);

        //Get work schedules of the employee for the current date
        $workSchedules =  WorkScheduleItem::where('employee_id', $employee->id)
            ->where('date', $date)->get();

        //Default work hours per employee
        $availableWorkHours = 9;

        //Calculate available work hours
        foreach ($workSchedules as $workSchedule) {
            $startTime = new \DateTime($workSchedule->start);
            $endTime = new \DateTime($workSchedule->end);
            $availableWorkHours = $availableWorkHours - (($endTime)->diff($startTime))->format("%h");
        }
        return response()->json(["available_work_hours" => $availableWorkHours]);
    }
    
    /**
     * Post Request Validation Rules
     *
     * @param Request $request
     * @return array
     */
    private function postRequestValidationRules()
    {
        $rules = [
            'name' => ['required', 'string', 'max:30'],
            'email' => ['required', 'string', 'email', 'max:30', 'unique:employee'],
            'password' => ['required', 'string', 'min:6'],
            'confirm-password' => ['required', 'string', 'min:6', 'same:password'],
        ];
        //TODO 
//        $requestUser = $request->user();
//        // Only admin user can set admin role.
//        if ($requestUser instanceof User && $requestUser->role === User::ADMIN_ROLE) {
//            $rules['role'] = 'in:BASIC_USER,ADMIN_USER';
//        } else {
//            $rules['role'] = 'in:BASIC_USER';
//        }
        return $rules;
    }
}
