<?php

namespace App\Services;

use App\Product;
use Validator;

class ProductService {
    public function createOrUpdate(Product $request) {
        $product = Product::where('product_ref_id', $request['product_ref_id'])->take(1)->first();
        if($product == null) {
            $product = Product::create([
                'name' => $request['name'],
                'product_ref_id' => $request['product_ref_id']
            ]);
        } else {
            $product->name = $request['name'];
            $product->save();
        }

        return $product;
    }
}
