<?php

namespace App\Services;

use App\Order;
use App\OrderItem;
use Illuminate\Http\Request;
use Validator;
use App\Services\OrderStatusService;

class OrderService
{
    private $orderStatusService;

    public function __construct(OrderStatusService $orderStatusService)
    {
        $this->orderStatusService = $orderStatusService;
    }

    public function getAll()
    {
        return Order::with('orderStatus')->get();
    }

    public function get($id)
    {
        return Order::with('orderStatus', 'orderItems', 'orderItems.orderItemStatus')->findOrFail($id);
    }

    public function create(array $request)
    {
        $validator = Validator::make($request, $this->postRequestValidationRules());
        $validator->validate();
        $order = Order::create([
            'due_date' => $request['due_date'],
            'order_ref_id' => $request['order_ref_id'],
            'status_id' => $this->orderStatusService->getStatusId($request['status']),
        ]);

        return $order;
        //TODO Create order. Status enquiry
        //TODO Create order items. Status new
        //TODO Create bill of materials when order status is changed to new. Status pending
    }

    public function update(array $request, $id)
    {
        $orderItemService = new OrderItemService(new OrderItemStatusService(), $this);
        $order = Order::with('orderStatus', 'orderItems.orderItemStatus')->find($id);
        if ($request['status'] != null) {
            $order->status_id = $this->orderStatusService->getStatusId($request['status']);
        }

        if ($request['status'] == "New") {
            $orderItems = OrderItem::where('order_id', $order->id)->get();
            foreach ($orderItems as $orderItem) {
                $orderItemService->updateOrderItemStatus($orderItem->id, "Pending");
            }
        }

        if ($request['status'] == "Cancelled") {
            $orderItems = OrderItem::where('order_id', $order->id)->get();
            foreach ($orderItems as $orderItem) {
                $orderItemService->updateOrderItemStatus($orderItem->id, $request['status']);
            }
        }
        $order = $order->save();
        return $order;
    }

    public function createOrUpdate(array $request) {
        $order = Order::where('order_ref_id', $request['order_ref_id'])->take(1)->first();
        if($order == null) {
            $order = Order::create([
                'due_date' => $request['due_date'],
                'order_ref_id' => $request['order_ref_id'],
                'status_id' => $this->orderStatusService->getStatusId($request['status']),
            ]);
        } else {
            if($order->status_id != $this->orderStatusService->getStatusId($request['status'])) {
                $this->update($request, $order->id);
            }
        }

        return $order;
    }

    public function updateOrderStatus($id, $status)
    {
        $order = Order::find($id);
        $order->status_id = $this->orderStatusService->getStatusId($status);
        $order->save();
        return $order;
    }

    /**
     * Post Request Validation Rules
     *
     * @param Request $request
     * @return array
     */
    private function postRequestValidationRules()
    {
        $rules = [
            'due_date' => 'required',
            'status' => 'required'
        ];
        //TODO 
//        $requestUser = $request->user();
//        // Only admin user can set admin role.
//        if ($requestUser instanceof User && $requestUser->role === User::ADMIN_ROLE) {
//            $rules['role'] = 'in:BASIC_USER,ADMIN_USER';
//        } else {
//            $rules['role'] = 'in:BASIC_USER';
//        }
        return $rules;
    }
}
