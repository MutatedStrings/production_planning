<?php

namespace App\Services;

use App\WorkScheduleItem;
use Validator;
use Illuminate\Http\Request;
use Unlu\Laravel\Api\QueryBuilder;

class WorkScheduleItemService {
    private $employeeService;

    public function __construct(EmployeeService $employeeService) {
        $this->employeeService = $employeeService;
    }

    public function getAll(Request $request) {
        $queryBuilder = new QueryBuilder(new WorkScheduleItem, $request);
        return $queryBuilder->build()->get();
    }

    public function get($id) {
        return WorkScheduleItem::with('orderItem', 'employee')->findOrFail($id);
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), $this->postRequestValidationRules());
        $validator->validate();
        $workScheduleItem =  WorkScheduleItem::create([
            'start' => $request->get('start'),
            'end' => $request->get('end'),
            'order_item_id' => $request->get('order_item_id'),
            'employee_id' => $request->get('employee_id'),
            'date' => $request->get('date'),
        ]);

        return $workScheduleItem;
    }

    public function update(Request $request, $id) {

    }

    /**
     * Post Request Validation Rules
     *
     * @param Request $request
     * @return array
     */
    private function postRequestValidationRules()
    {
        $rules = [
            'start' => 'required',
            'end' => 'required',
            'order_item_id' => ['required', 'string', 'max:6'],
            'employee_id' => ['required', 'string', 'max:6'],
            'date' => 'required',
        ];
        //TODO
//        $requestUser = $request->user();
//        // Only admin user can set admin role.
//        if ($requestUser instanceof User && $requestUser->role === User::ADMIN_ROLE) {
//            $rules['role'] = 'in:BASIC_USER,ADMIN_USER';
//        } else {
//            $rules['role'] = 'in:BASIC_USER';
//        }
        return $rules;
    }
}
