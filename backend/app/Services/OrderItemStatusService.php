<?php

namespace App\Services;

use App\OrderItemStatus;
use Illuminate\Http\Request;

class OrderItemStatusService {
    public function getAll() {
        return OrderItemStatus::all();
    }
    
    public function get($id) {
        return OrderItemStatus::findOrFail($id);
    }
    
    public function getStatusId($status) {
        return OrderItemStatus::where('status', $status)->first()->id;
    }
}