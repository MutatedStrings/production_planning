<?php

namespace App\Services;

use App\OrderStatus;
use Illuminate\Http\Request;
use Validator;

class OrderStatusService {
    public function getAll() {
        return OrderStatus::all();
    }
    
    public function get($id) {
        return OrderStatus::findOrFail($id);
    }
    
    public function getStatusId($status) {
        return OrderStatus::where('status', $status)->first()->id;
    }
}