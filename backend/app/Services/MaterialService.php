<?php

namespace App\Services;

use App\Material;
use Validator;

class MaterialService {
    public function createOrUpdate(Material $request) {
        $material = Material::where('material_ref_id', $request['material_ref_id'])->take(1)->first();
        if($material == null) {
            $material = Material::create([
                'name' => $request['name'],
                'material_ref_id' => $request['material_ref_id']
            ]);
        } else {
            $material->name = $request['name'];
            $material->save();
        }

        return $material;
    }
}
