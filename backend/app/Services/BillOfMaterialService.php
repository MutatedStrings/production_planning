<?php

namespace App\Services;

use App\BillItem;
use App\BillOfMaterial;
use App\ProductStructure;
use App\ProductStructureItem;
use Illuminate\Http\Request;
use Validator;
use App\Services\BomStatusService;
use App\Services\OrderItemService;
use Unlu\Laravel\Api\QueryBuilder;

class BillOfMaterialService {
    private $bomStatusService;
    private $orderItemService;
    
    public function __construct(BomStatusService $bomStatusService, OrderItemService $orderItemService) {
        $this->bomStatusService = $bomStatusService;
        $this->orderItemService = $orderItemService;
    }
    
    public function getAll(Request $request) {
        $queryBuilder = new QueryBuilder(new BillOfMaterial, $request);
        
        return $queryBuilder->build()->get();
    }
    
    public function get($id) {
        return BillOfMaterial::with('bomStatus', 'orderItem', 'billItems')->findOrFail($id);
    }
    
    public function create($orderItemId, $product_id, $quantity) {
        $billOfMaterial = BillOfMaterial::create([
            'order_item_id' => $orderItemId,
            'status_id' => $this->bomStatusService->getStatusId("Pending")
        ]);

        $productStructureId = ProductStructure::where('product_id', $product_id)->take(1)->first()->id;
        $productStructureItems = ProductStructureItem::where('structure_id', $productStructureId)->get();

        foreach ($productStructureItems as $productStructureItem) {
            BillItem::create([
                'bom_id' => $billOfMaterial->id,
                'material_id' => $productStructureItem->material_id,
                'quantity' => $quantity*($productStructureItem->quantity)
            ]);
        }

        return $billOfMaterial;
    }
    
    public function update(Request $request, $id) {
        $billOfMaterial = BillOfMaterial::find($id);
        
        if($request->get('status') != null) {
            $billOfMaterial->status_id = $this->bomStatusService->getStatusId($request->get('status'));
        }
        
        $billOfMaterial->save();
        
        // Once bom is approved order item status is changed to Pending
        if($request->get('status') == 'Approved') {
            $this->orderItemService->updateOrderItemStatus($billOfMaterial->order_item_id, "Pending");
        }
        
        // Start order item production once all bom items are received
        // Order item status is changed to In Progress
        if($request->get('status') == 'Received') {
            $this->orderItemService->updateOrderItemStatus($billOfMaterial->order_item_id, "In Progress");
        }
        return $billOfMaterial;
    }
    
    public function updateBomStatus($id, $status) {
        $billOfMaterial = BillOfMaterial::find($id);
        $billOfMaterial->status_id = $this->bomStatusService->getStatusId($status);
        $billOfMaterial->save();
        
        // Once bom is approved order item status is changed to Pending
        if($status == 'Approved') {
            $this->orderItemService->updateOrderItemStatus($billOfMaterial->order_item_id, "Pending");
        }
        
        // Start order item production once all bom items are received
        // Order item status is changed to In Progress
        if($status == 'Received') {
            $this->orderItemService->updateOrderItemStatus($billOfMaterial->order_item_id, "In Progress");
        }
        
        return $billOfMaterial;
    }
    
    /**
     * Post Request Validation Rules
     *
     * @param Request $request
     * @return array
     */
    private function postRequestValidationRules()
    {
        $rules = [
            'order_item_id'     => 'required',
            'status'            => 'required'
        ];
        //TODO 
//        $requestUser = $request->user();
//        // Only admin user can set admin role.
//        if ($requestUser instanceof User && $requestUser->role === User::ADMIN_ROLE) {
//            $rules['role'] = 'in:BASIC_USER,ADMIN_USER';
//        } else {
//            $rules['role'] = 'in:BASIC_USER';
//        }
        return $rules;
    }
}
