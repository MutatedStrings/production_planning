<?php

namespace App\Services;

use App\CustomFilters\OrderItemQueryBuilder;
use App\Order;
use App\OrderItem;
use App\Product;
use App\WorkScheduleItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class OrderItemService {
    private $orderItemStatusService;
    private $orderService;
    
    public function __construct(OrderItemStatusService $orderItemStatusService, OrderService $orderService) {
        $this->orderItemStatusService = $orderItemStatusService;
        $this->orderService = $orderService;
    }
    
    public function getAll(Request $request) {
        $queryBuilder = new OrderItemQueryBuilder(new OrderItem, $request);
        return $queryBuilder->build()->get();
    }
    
    public function get($id) {
        return OrderItem::with('product', 'order.orderStatus', 'orderItemStatus')->findOrFail($id);
    }
    
    public function create(Request $request) {
        $validator = Validator::make($request->all(), $this->postRequestValidationRules());
        $validator->validate();
        
        $orderItem = OrderItem::create([
            'quantity'   => $request->get('quantity'),
            'product_id' => $request->get('product_id'),
            'order_id'   => $request->get('order_id'),
            'status_id'  => $request->get('status_id')
        ]);
        
        return $orderItem;
    }
    
    public function update(array $request, $id) {
        $orderItem = OrderItem::with('product', 'order.orderStatus', 'orderItemStatus')->find($id);
        if($request['status'] != null) {
            $orderItem->status_id = $this->orderItemStatusService->getStatusId($request['status']);
        }
        if($request->get('quantity') != null) {
            $orderItem->quantity = $request->get('quantity');
        }

        // Create BOM and Bom items when order item status is set to Pending
        if($request['status'] == "Pending") {
            $billOfMaterialService = app()->make(BillOfMaterialService::class);
            $billOfMaterialService->create($orderItem->id, $orderItem->product_id, $orderItem->quantity);
        }

        // Update Bom status to Cancelled if order item is cancelled
        if($request['status'] == "Cancelled") {
            $billOfMaterial = $orderItem->billOfMaterials()->first();
            $billOfMaterialService = app()->make(BillOfMaterialService::class);
            $billOfMaterialService->updateBomStatus($billOfMaterial->id, "Cancelled");
        }

        // Check if status of order item changed to in progress and if order is in New change to in Progress
        if($request['status'] == "In Progress") {
            $order = $orderItem->order;
            if($order->orderStatus->status == "New") {
                $this->orderService->updateOrderStatus($order->id, "In Progress");
            }
        }

        if($request['status'] == "Complete") {
            $orderItem->completion_date = DB::raw('CURRENT_TIMESTAMP');
            $orderItem->save();
            $orderItems = OrderItem::where('order_id', $orderItem->order_id)->get();
            $allEqual = false;
            foreach ($orderItems as $newOrderItem) {
                if($newOrderItem->orderItemStatus->status != "Complete" && $newOrderItem->id != $orderItem->id) {
                    $allEqual = false;
                    break;
                } else {
                    $allEqual = true;
                }
            }

            if($allEqual) {
                $this->orderService->updateOrderStatus($orderItem->order_id, $request['status']);
            }
        }

        $orderItem->save();
        return $orderItem;
    }
    
    public function updateOrderItemStatus($id, $status) {
        $orderItem = OrderItem::find($id);
        $orderItem->status_id = $this->orderItemStatusService->getStatusId($status);
        $orderItem->save();

        // Create BOM and Bom items when order item status is set to Pending
        if($status == "Pending") {
            $billOfMaterialService = app()->make(BillOfMaterialService::class);
            $billOfMaterialService->create($orderItem->id, $orderItem->product_id, $orderItem->quantity);
        }

        // Update Bom status to Cancelled if order item is cancelled
        if($status == "Cancelled") {
            $billOfMaterial = $orderItem->billOfMaterials()->first();
            $billOfMaterialService = app()->make(BillOfMaterialService::class);
            $billOfMaterialService->updateBomStatus($billOfMaterial->id, "Cancelled");
        }

        // Check if status of order item changed to in progress and if order is in New change to in Progress
        if($status == "In Progress") {
            $order = $orderItem->order;
            if($order->orderStatus->status == "New") {
                $this->orderService->updateOrderStatus($order->id, "In Progress");
            }
        }

        // If all order items are complete change order status to complete
        if($status == "Complete") {
            $orderItem->completion_date = DB::raw('CURRENT_TIMESTAMP');
            $orderItem->save();
            $orderItems = OrderItem::where('order_id', $orderItem->order_id)->get();
            $allEqual = false;
            foreach ($orderItems as $newOrderItem) {
                if($newOrderItem->orderItemStatus->status != "Complete" && $newOrderItem->id != $orderItem->id) {
                    $allEqual = false;
                    break;
                } else {
                    $allEqual = true;
                }
            }

            if($allEqual) {
                $this->orderService->updateOrderStatus($orderItem->order_id, "Complete");
            }
        }

        return $orderItem;
    }

    public function createOrUpdate(array $request) {
        $orderItem = OrderItem::where('order_item_ref_id', $request['order_item_ref_id'])->take(1)->first();
        if($orderItem == null) {
            $orderItem = OrderItem::create([
                'quantity' => $request['quantity'],
                'product_id' => Product::where('product_ref_id', $request['product_ref_id'])->take(1)->first()->id,
                'order_id' => Order::where('order_ref_id', $request['order_ref_id'])->take(1)->first()->id,
                'order_item_ref_id' => $request['order_item_ref_id'],
                'status_id' => $this->orderItemStatusService->getStatusId($request['status']),
            ]);
            // Create BOM and Bom items when order item status is set to Pending
            if($request['status'] == "Pending") {
                $billOfMaterialService = app()->make(BillOfMaterialService::class);
                $billOfMaterialService->create($orderItem->id, $orderItem->product_id, $orderItem->quantity);
            }
        } else {
            if($orderItem->status_id != $this->orderItemStatusService->getStatusId($request['status'])) {
                $this->update($request, $orderItem->id);
            }
        }

        return $orderItem;
    }

    public function remainingHours($id) {
        $orderItem = OrderItem::with('product.productStructure')->findOrFail($id);

        //Get all work schedules that contain the order item
        $workSchedules =  WorkScheduleItem::where('order_item_id', $orderItem->id)->get();

        //Get the estimated completion time for the product
        $estimatedCompletionHours = (double) $orderItem->product->productStructure->estimated_completion_hours;
        $totalHoursPlanned = 0;

        //Calculate available work hours
        foreach ($workSchedules as $workSchedule) {
            $startTime = new \DateTime($workSchedule->start);
            $endTime = new \DateTime($workSchedule->end);
            $totalHoursPlanned = $totalHoursPlanned + (($endTime)->diff($startTime))->format("%h");
        }
        return response()->json([
            "total_hours_planned" => $totalHoursPlanned,
            "estimated_completion_hours" => $estimatedCompletionHours
        ]);
    }

    /**
     * Post Request Validation Rules
     *
     * @param Request $request
     * @return array
     */
    private function postRequestValidationRules()
    {
        $rules = [
            'quantity'          => 'required',
            'product_id'        => 'required',
            'order_id'          => 'required',
            'status_id'         => 'required'
        ];
        //TODO 
//        $requestUser = $request->user();
//        // Only admin user can set admin role.
//        if ($requestUser instanceof User && $requestUser->role === User::ADMIN_ROLE) {
//            $rules['role'] = 'in:BASIC_USER,ADMIN_USER';
//        } else {
//            $rules['role'] = 'in:BASIC_USER';
//        }
        return $rules;
    }
}
