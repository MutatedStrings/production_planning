<?php

namespace App\Services;

use App\BillItem;
use Illuminate\Http\Request;
use Validator;

class BillItemService {
    public function getAll() {
        return BillItem::with('material', 'billOfMaterial')->all();
    }
    
    public function get($id) {
        return BillItem::with('material', 'billOfMaterial')->findOrFail($id);
    }
    
    public function create(Request $request) {
        $validator = Validator::make($request->all(), $this->postRequestValidationRules());
        $validator->validate();
        
        $billItem = BillItem::create([
            'bom_id' => $request->get('bom_id'),
            'material_id' => $request->get('material_id'),
            'quantity' => $request->get('quantity')
        ]);
        
        return $billItem;
    }
    
    public function update(Request $request, $id) {
        $billItem = BillItem::find($id);
        $billItem->update($billItem, $request->all());
        return $billItem;
    }
    
    /**
     * Post Request Validation Rules
     *
     * @param Request $request
     * @return array
     */
    private function postRequestValidationRules()
    {
        $rules = [
            'bom_id'        => 'required',
            'material_id'   => 'required',
            'quantity'      => 'required'
        ];
        //TODO 
//        $requestUser = $request->user();
//        // Only admin user can set admin role.
//        if ($requestUser instanceof User && $requestUser->role === User::ADMIN_ROLE) {
//            $rules['role'] = 'in:BASIC_USER,ADMIN_USER';
//        } else {
//            $rules['role'] = 'in:BASIC_USER';
//        }
        return $rules;
    }
}
