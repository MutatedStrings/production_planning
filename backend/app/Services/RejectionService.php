<?php

namespace App\Services;

use App\RejectionNotice;
use Illuminate\Http\Request;
use Validator;
use App\Services\InspectionService;
use App\Services\OrderItemService;
use Unlu\Laravel\Api\QueryBuilder;

class RejectionService {
    private $inspectionService;
    private $orderItemService;
    
    public function __construct(InspectionService $inspectionService, OrderItemService $orderItemService) {
        $this->inspectionService = $inspectionService;
        $this->orderItemService = $orderItemService;
    }
    
    public function getAll(Request $request) {
        $queryBuilder = new QueryBuilder(new RejectionNotice, $request);
        
        return $queryBuilder->build()->get();
    }
    
    public function get($id) {
        return RejectionNotice::with('inspection')->findOrFail($id);
    }
    /*public function make(Request $request)
    {
        
        $validator = Validator::make($request->all(), $this->postRequestValidationRules());
        $validator->validate();
        $rejectionNotice = RejectionNotice::create([
           'rejection_cause' => $request->get('rejection_cause'),
        ]);
        return $rejectionNotice; 
    }*/
    
    public function createRejectionNotice(Request $request) {
        $validator = Validator::make($request->all(), $this->postRequestValidationRules());
        $validator->validate();
        $inspection = $this->inspectionService->create($request);
        
        $rejectionNotice = new RejectionNotice([
            'rejection_cause' => $request->get('rejection_cause')
        ]);
        
        $inspection->rejectionNotice()->save($rejectionNotice);
        
        $orderItemId = $inspection->order_item_id;
        $this->orderItemService->updateOrderItemStatus($orderItemId, "Rejected");
        return $rejectionNotice;
    }
    
    public function update(Request $request, $id) {
        $rejectionNotice = RejectionNotice::find($id);
        if($request->get('action') != null) {
            $rejectionNotice->action = $request->get('action');
        }
        $rejectionNotice->save();
        return $rejectionNotice;
    }
    
    /**
     * Post Request Validation Rules
     *
     * @param Request $request
     * @return array
     */
    private function postRequestValidationRules()
    {
        $rules = [
            'rejection_cause'       => 'required|max:100'
        ];
        //TODO 
//        $requestUser = $request->user();
//        // Only admin user can set admin role.
//        if ($requestUser instanceof User && $requestUser->role === User::ADMIN_ROLE) {
//            $rules['role'] = 'in:BASIC_USER,ADMIN_USER';
//        } else {
//            $rules['role'] = 'in:BASIC_USER';
//        }
        return $rules;
    }
}
