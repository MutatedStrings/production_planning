<?php

namespace App\Services;

use App\BomStatus;
use Illuminate\Http\Request;

class BomStatusService {
    public function getAll() {
        return BomStatus::all();
    }
    
    public function get($id) {
        return BomStatus::findOrFail($id);
    }
    
    public function getStatusId($status) {
        return BomStatus::where('status', $status)->first()->id;
    }
}