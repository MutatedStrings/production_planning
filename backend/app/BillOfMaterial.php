<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $order_item_id
 * @property int $status_id
 * @property string $created_at
 * @property string $updated_at
 * @property BomStatus $bomStatus
 * @property OrderItem $orderItem
 * @property BillItem[] $billItems
 */
class BillOfMaterial extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new bill of material object is being created
        static::creating(function ($billOfMaterial) {
            $lastBom = BillOfMaterial::orderBy('id', 'desc')->take(1)->first();
            $lastBomId = null;
            $newBillOfMaterialId = null;
            if ($lastBom) {
                $lastBomId = $lastBom->id;
                $newBillOfMaterialId = ++$lastBomId;
            } else {
                $newBillOfMaterialId = 'B00001';
            }
            $billOfMaterial->{$billOfMaterial->getKeyName()} = $newBillOfMaterialId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'bill_of_material';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['order_item_id', 'status_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bomStatus()
    {
        return $this->belongsTo('App\BomStatus', 'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderItem()
    {
        return $this->belongsTo('App\OrderItem');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function billItems()
    {
        return $this->hasMany('App\BillItem', 'bom_id');
    }
}
