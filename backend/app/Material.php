<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $name
 * @property int $material_ref_id
 * @property string $created_at
 * @property string $updated_at
 * @property BillItem[] $billItems
 * @property ProductStructureItem[] $productStructureItems
 */
class Material extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new material object is being created
        static::creating(function ($material) {
            $lastMaterial = Material::orderBy('id', 'desc')->take(1)->first();
            $lastMaterialId = null;
            $newMaterialId = null;
            if ($lastMaterial) {
                $lastMaterialId = $lastMaterial->id;
                $newMaterialId = ++$lastMaterialId;
            } else {
                $newMaterialId = 'M00001';
            }
            $material->{$material->getKeyName()} = $newMaterialId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'material';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['name', 'material_ref_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function billItems()
    {
        return $this->hasMany('App\BillItem');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productStructureItems()
    {
        return $this->hasMany('App\ProductStructureItem');
    }
}
