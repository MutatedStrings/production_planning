<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;

/**
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $contact_number
 * @property string $employee_type
 * @property string $created_at
 * @property string $updated_at
 * @property Inspection[] $inspections
 * @property Leave[] $leaves
 * @property WorkScheduleItem[] $workScheduleItems
 */
class Employee extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;
    
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new employee object is being created
        static::creating(function ($employee) {
            $lastEmployee = Employee::orderBy('id', 'desc')->take(1)->first();
            $lastEmployeeId = null;
            $newEmployeeId = null;
            if ($lastEmployee) {
                $lastEmployeeId = $lastEmployee->id;
                $newEmployeeId = ++$lastEmployeeId;
            } else {
                $newEmployeeId = 'E00001';
            }
            $employee->{$employee->getKeyName()} = $newEmployeeId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'employee';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'contact_number', 'employee_type', 'created_at', 'updated_at'];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inspections()
    {
        return $this->hasMany('App\Inspection', 'inspector_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leaves()
    {
        return $this->hasMany('App\Leave');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workScheduleItems()
    {
        return $this->hasMany('App\WorkScheduleItem');
    }
}
