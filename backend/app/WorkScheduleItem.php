<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string start
 * @property string end
 * @property string $order_item_id
 * @property string $employee_id
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 * @property OrderItem $orderItem
 * @property Employee $employee
 */
class WorkScheduleItem extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new work schedule item object is being created
        static::creating(function ($workScheduleItem) {
            $lastWorkScheduleItem = WorkScheduleItem::orderBy('id', 'desc')->take(1)->first();
            $lastWorkScheduleItemId = null;
            $newWorkScheduleItemId = null;
            if ($lastWorkScheduleItem) {
                $lastWorkScheduleItemId = $lastWorkScheduleItem->id;
                $newWorkScheduleItemId = ++$lastWorkScheduleItemId;
            } else {
                $newWorkScheduleItemId = 'WS0001';
            }
            $workScheduleItem->{$workScheduleItem->getKeyName()} = $newWorkScheduleItemId;
        });
    }
    
    /**
     * The table associated with the model.x
     * 
     * @var string
     */
    protected $table = 'work_schedule_item';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['start', 'end', 'order_item_id', 'employee_id', 'date', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderItem()
    {
        return $this->belongsTo('App\OrderItem');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
