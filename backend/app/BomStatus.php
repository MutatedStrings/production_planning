<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $status
 * @property BillOfMaterial[] $billOfMaterials
 */
class BomStatus extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'bom_status';

    /**
     * @var array
     */
    protected $fillable = ['status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function billOfMaterials()
    {
        return $this->hasMany('App\BillOfMaterial', 'status_id');
    }
}
