<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $leave_code
 * @property string $type
 * @property Leave[] $leaves
 */
class LeaveType extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'leave_type';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'leave_code';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leaves()
    {
        return $this->hasMany('App\Leave', 'code', 'leave_code');
    }
}
