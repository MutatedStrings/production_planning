<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $structure_id
 * @property string $material_id
 * @property int $quantity
 * @property string $created_at
 * @property string $updated_at
 * @property Material $material
 * @property ProductStructure $productStructure
 */
class ProductStructureItem extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new product structure item object is being created
        static::creating(function ($productStructureItem) {
            $lastProductStructureItem = ProductStructureItem::orderBy('id', 'desc')->take(1)->first();
            $lastProductStructureItemId = null;
            $newProductStructureItemId = null;
            if ($lastProductStructureItem) {
                $lastProductStructureItemId = $lastProductStructureItem->id;
                $newProductStructureItemId = ++$lastProductStructureItemId;
            } else {
                $newProductStructureItemId = 'SI0001';
            }
            $productStructureItem->{$productStructureItem->getKeyName()} = $newProductStructureItemId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'product_structure_item';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['structure_id', 'material_id', 'quantity', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function material()
    {
        return $this->belongsTo('App\Material');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productStructure()
    {
        return $this->belongsTo('App\ProductStructure', 'structure_id');
    }
}
