<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $inspector_id
 * @property string $order_item_id
 * @property string $inspection_date
 * @property string $inspection_result
 * @property string $created_at
 * @property string $updated_at
 * @property OrderItem $orderItem
 * @property Employee $employee
 * @property RejectionNotice $rejectionNotice
 */
class Inspection extends Model
{
    protected static function boot() {
        parent::boot();
        
        // Triggered when a new inspection object is being created
        static::creating(function ($inspection) {
            $lastInspection = Inspection::orderBy('id', 'desc')->take(1)->first();
            $lastInspectionId = null;
            $newInspectionId = null;
            if ($lastInspection) {
                $lastInspectionId = $lastInspection->id;
                $newInspectionId = ++$lastInspectionId;
            } else {
                $newInspectionId = 'I00001';
            }
            $inspection->{$inspection->getKeyName()} = $newInspectionId;
        });
    }
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'inspection';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['inspector_id', 'order_item_id', 'inspection_date', 'inspection_result', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderItem()
    {
        return $this->belongsTo('App\OrderItem');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee', 'inspector_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function rejectionNotice()
    {
        return $this->hasOne('App\RejectionNotice');
    }
}
