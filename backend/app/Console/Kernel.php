<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\GetSalesOrders::class,
        Commands\GetMaterials::class,
        Commands\GetProducts::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sync:materials')->everyFiveMinutes();
        $schedule->command('sync:products')->everyFiveMinutes();
        $schedule->command('sync:sales')->everyFiveMinutes();
    }
}
