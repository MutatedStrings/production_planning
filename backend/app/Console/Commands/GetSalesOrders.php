<?php
namespace App\Console\Commands;
use App\Services\OrderItemService;
use App\Services\OrderService;
use Illuminate\Console\Command;

class GetSalesOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get updates for sales orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orderService = app()->make(OrderService::class);
        $orderItemService = app()->make(OrderItemService::class);
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->request('GET', 'http://eirls.herokuapp.com/productionOrders');
            $orders = json_decode($response->getBody()->getContents());
            foreach ($orders as $order) {
                $newOrder = array();
                $orderItems = $order->ord;
                $dueDate = $order->due_date;
                $time = null;

                // Create new order
                if($dueDate == null) {
                    $dueDate = $order->date_placed;
                    $time = strtotime($dueDate. + ' + 25 days');
                } else {
                    $time = strtotime($dueDate);
                }
                $formattedDate = date('Y-m-d', $time);
                $newOrder['due_date'] = $formattedDate;
                if($order->order_status == 'pending') {
                    $newOrder['status'] = 'Enquiry';
                } else if($order->order_status == 'confirmed') {
                    $newOrder['status'] = 'New';
                } else {
                    $newOrder['status'] = 'Cancelled';
                }
                $newOrder['order_ref_id'] = $order->order_id;

                // Add order items
                $newOrderItems = array();
                foreach ($orderItems as $orderItem) {
                    if(($orderItem->product_status == 'Production' || $orderItem->product_status == 'cancelled')
                        && $orderItem->product_type == 'Finished Good') {
                        $newOrderItem = null;
                        $newOrderItem['quantity'] = $orderItem->product_quantity;
                        $newOrderItem['product_ref_id'] = $orderItem->material_order_id;
                        $newOrderItem['order_ref_id'] = $order->order_id;
                        $newOrderItem['order_item_ref_id'] = $orderItem->orderitems_id;
                        if($newOrder['status'] == 'Enquiry') {
                            $newOrderItem['status'] = 'New';
                        } elseif ($newOrder['status'] == 'New') {
                            $newOrderItem['status'] = 'Pending';
                        } else {
                            $newOrderItem['status'] = 'Cancelled';
                        }

                        array_push($newOrderItems, $newOrderItem);
                    }
                }

                if(sizeof($newOrderItems) != 0) {
                    $orderService->createOrUpdate($newOrder);
                    foreach ($newOrderItems as $newOrderItem) {
                        $orderItemService->createOrUpdate($newOrderItem);
                    }
                }
            }
            $this->info('Orders table synced');
        } catch (\GuzzleHttp\Exception\GuzzleException $ex) {
            $this->error($ex->getMessage());
        }
    }
}
