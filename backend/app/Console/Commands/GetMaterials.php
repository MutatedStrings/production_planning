<?php

namespace App\Console\Commands;

use App\Material;
use App\Services\MaterialService;
use Illuminate\Console\Command;

class GetMaterials extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:materials';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get updates for material management materials';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $materialService = new MaterialService();
        $client = new \GuzzleHttp\Client();
        try {
            $headers = ['headers' => ['Authorization' => 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJleHRlcm5hbCIsImlhdCI6MTU1NTMyNjk2OSwiZXhwIjoxNTU1NDEzMzY5fQ.kDnlreG8p_VcoLh3FVrZI3a8go4IXQCWHBMIGJxNOaMeKsrhPz-Axv3RWiXgsxbQNXmXc4HTx7IQ9622Z20RZw']];
            $response = $client->request('GET', 'https://eirls-mm.herokuapp.com/api/items-raw', $headers);
            $materials = json_decode($response->getBody()->getContents());
            foreach ($materials as $material) {
                $newMaterial = new Material();
                $newMaterial->name = $material->name;
                $newMaterial->material_ref_id = $material->id;
                $materialService->createOrUpdate($newMaterial);
            }
            $this->info('Materials table synced');
        } catch (\GuzzleHttp\Exception\GuzzleException $ex) {
            $this->error($ex->getMessage());
        }
    }
}
