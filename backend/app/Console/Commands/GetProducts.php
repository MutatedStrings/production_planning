<?php
namespace App\Console\Commands;
use App\Product;
use App\Services\ProductService;
use Illuminate\Console\Command;

class GetProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get updates for material management products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productService = new ProductService();
        $client = new \GuzzleHttp\Client();
        try {
            $headers = ['headers' => ['Authorization' => 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJleHRlcm5hbCIsImlhdCI6MTU1NTMyNjk2OSwiZXhwIjoxNTU1NDEzMzY5fQ.kDnlreG8p_VcoLh3FVrZI3a8go4IXQCWHBMIGJxNOaMeKsrhPz-Axv3RWiXgsxbQNXmXc4HTx7IQ9622Z20RZw']];
            $response = $client->request('GET', 'https://eirls-mm.herokuapp.com/api/items-complete', $headers);
            $products = json_decode($response->getBody()->getContents());
            foreach ($products as $product) {
                $newProduct = new Product();
                $newProduct->name = $product->name;
                $newProduct->product_ref_id = $product->id;
                $productService->createOrUpdate($newProduct);
            }
            $this->info('Products table synced');
        } catch (\GuzzleHttp\Exception\GuzzleException $ex) {
            $this->error($ex->getMessage());
        }
    }
}
