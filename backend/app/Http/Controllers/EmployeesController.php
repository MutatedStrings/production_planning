<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Employee;
use App\Services\EmployeeService;

class EmployeesController extends Controller
{
    private $employeeService;
    
    public function __construct(EmployeeService $employeeService) {
        $this->employeeService = $employeeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return EmployeeService[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        return $this->employeeService->getAll($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->employeeService->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->employeeService->get($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function availableWorkHours($id, $date) {
        return $this->employeeService->getAvailableWorkHours($id, $date);
    }
}
