<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderItem;
use Unlu\Laravel\Api\QueryBuilder;
use App\CustomFilters\OrderItemQueryBuilder;
use App\Services\OrderItemService;

class OrderItemsController extends Controller
{
    private $orderItemService;
    
    public function __construct(OrderItemService $orderItemService) {
        $this->orderItemService = $orderItemService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->orderItemService->getAll($request);
//        $queryBuilder = new OrderItemQueryBuilder(new OrderItem, $request);
//        
//        return $queryBuilder->build()->paginate();
//        return OrderItem::with('product', 'order.orderStatus', 'orderItemStatus')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->orderItemService->get($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\ZHttp\Response
     */
    public function update(Request $request, $id)
    {
        return $this->orderItemService->update($request->all(), $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function remainingHours($id) {
        return $this->orderItemService->remainingHours($id);
    }
}
