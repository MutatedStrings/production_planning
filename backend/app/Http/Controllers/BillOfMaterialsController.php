<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BillOfMaterial;
use App\Services\BillOfMaterialService;

class BillOfMaterialsController extends Controller
{
    private $billOfMaterialService;
    
    public function __construct(BillOfMaterialService $billOfMaterialService) {
        $this->billOfMaterialService = $billOfMaterialService;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*if($request['include'] == 'billItems') {
            return BillOfMaterial::with('bomStatus', 'orderItem', 'billItems')->paginate();
        } else {
            return BillOfMaterial::with('bomStatus', 'orderItem')->paginate();
        }*/
        return $this->billOfMaterialService->getAll($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->billOfMaterialService->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->billOfMaterialService->get($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->billOfMaterialService->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
