<?php

namespace App\Http\Controllers;

use App\Services\WorkScheduleItemService;
use Illuminate\Http\Request;

class WorkScheduleItemsController extends Controller
{
    private $workScheduleItemService;

    public function __construct(WorkScheduleItemService $workScheduleItemService) {
        $this->workScheduleItemService = $workScheduleItemService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return WorkScheuleItemService
     */
    public function index(Request $request)
    {
        return $this->workScheduleItemService->getAll($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->workScheduleItemService->create($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->workScheduleItemService->get($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
