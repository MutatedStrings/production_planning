<?php
return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'employee',
    ],

    'guards' => [
        'api' => [
            'driver' => 'passport',
            'provider' => 'employee',
        ],
    ],

    'providers' => [
        'employee' => [
            'driver' => 'eloquent',
            'model' => \App\Employee::class
        ]
    ]
];