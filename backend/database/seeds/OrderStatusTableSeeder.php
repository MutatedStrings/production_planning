<?php

use Illuminate\Database\Seeder;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_status')->insert([
            'status' => 'Enquiry'
        ]);
        
        DB::table('order_status')->insert([
            'status' => 'New'
        ]);
        
        DB::table('order_status')->insert([
            'status' => 'In Progress'
        ]);
        
        DB::table('order_status')->insert([
            'status' => 'Complete'
        ]);
        
        DB::table('order_status')->insert([
            'status' => 'Cancelled'
        ]);
    }
}
