<?php

use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('order')->insert([
            'id' => 'C00001',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'status_id' => 1,
            'order_ref_id' => 1,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00002',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'status_id' => 2,
            'order_ref_id' => 2,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00003',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'status_id' => 2,
            'order_ref_id' => 3,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00004',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'status_id' => 3,
            'order_ref_id' => 4,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00005',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'status_id' => 1,
            'order_ref_id' => 5,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00006',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'status_id' => 1,
            'order_ref_id' => 6,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 25 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00007',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 10 DAY)'),
            'status_id' => 2,
            'order_ref_id' => 7,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 9 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00008',
            'due_date' => DB::raw('CURRENT_TIMESTAMP'),
            'status_id' => 4,
            'order_ref_id' => 8,
            'projected_completion_date' => DB::raw('CURRENT_TIMESTAMP'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00009',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 20 DAY)'),
            'status_id' => 5,
            'order_ref_id' => 9,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 20 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order')->insert([
            'id' => 'C00010',
            'due_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 29 DAY)'),
            'status_id' => 2,
            'order_ref_id' => 10,
            'projected_completion_date' => DB::raw('DATE_ADD(NOW(), INTERVAL 28 DAY)'),
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
    }
}
