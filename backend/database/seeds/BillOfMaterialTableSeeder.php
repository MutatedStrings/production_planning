<?php

use Illuminate\Database\Seeder;

class BillOfMaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bill_of_material')->insert([
            'id' => 'B00001',
            'order_item_id' => 'PO0001',
            'status_id' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_of_material')->insert([
            'id' => 'B00002',
            'order_item_id' => 'PO0002',
            'status_id' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_of_material')->insert([
            'id' => 'B00003',
            'order_item_id' => 'PO0003',
            'status_id' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
