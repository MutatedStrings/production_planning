<?php

use Illuminate\Database\Seeder;

class MaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('material')->insert([
            'id' => 'M00001',
            'name' => 'Hummer Frame 1',
             'material_ref_id' => 1,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00002',
            'name' => 'Hummer Frame 2',
            'material_ref_id' => 2,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00003',
            'name' => 'Hummer Wheel 1',
            'material_ref_id' => 3,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00004',
            'name' => 'Hummer Pedal 1',
            'material_ref_id' => 4,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00005',
            'name' => 'Scooty Frame 1',
            'material_ref_id' => 5,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00006',
            'name' => 'Scooty Wheel 1',
            'material_ref_id' => 6,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00007',
            'name' => 'Scooty Pedal 1',
            'material_ref_id' => 7,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00008',
            'name' => 'Yamaha Apache Frame 1',
            'material_ref_id' => 8,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00009',
            'name' => 'Yamaha Apache Wheel 1',
            'material_ref_id' => 9,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00010',
            'name' => 'Yamaha Apache Pedal 1',
            'material_ref_id' => 10,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00011',
            'name' => 'Fuji Frame 1',
            'material_ref_id' => 11,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00012',
            'name' => 'Fuji Wheel 1',
            'material_ref_id' => 12,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('material')->insert([
            'id' => 'M00013',
            'name' => 'Fuji Pedal 1',
            'material_ref_id' => 13,
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
