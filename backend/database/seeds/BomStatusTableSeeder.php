<?php

use Illuminate\Database\Seeder;

class BomStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bom_status')->insert([
            'status' => 'Pending'
        ]);
        
        DB::table('bom_status')->insert([
            'status' => 'Approved'
        ]);
        
        DB::table('bom_status')->insert([
            'status' => 'Cancelled'
        ]);
        
        DB::table('bom_status')->insert([
            'status' => 'Dispatched'
        ]);
        
        DB::table('bom_status')->insert([
            'status' => 'Received'
        ]);
    }
}
