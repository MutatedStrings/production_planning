<?php

use Illuminate\Database\Seeder;

class OrderItemStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_item_status')->insert([
            'status' => 'New'
        ]);
        
        DB::table('order_item_status')->insert([
            'status' => 'Pending'
        ]);
        
        DB::table('order_item_status')->insert([
            'status' => 'In Progress'
        ]);
        
        DB::table('order_item_status')->insert([
            'status' => 'Awaiting Inspection'
        ]);
        
        DB::table('order_item_status')->insert([
            'status' => 'Cancelled'
        ]);
        
        DB::table('order_item_status')->insert([
            'status' => 'Rejected'
        ]);
        
        DB::table('order_item_status')->insert([
            'status' => 'Complete'
        ]);
    }
}
