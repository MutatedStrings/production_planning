<?php

use Illuminate\Database\Seeder;

class BillItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bill_item')->insert([
            'id' => 'BI0001',
            'bom_id' => 'B00001',
            'material_id' => 'M00001',
            'quantity' => 20,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_item')->insert([
            'id' => 'BI0002',
            'bom_id' => 'B00001',
            'material_id' => 'M00003',
            'quantity' => 40,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_item')->insert([
            'id' => 'BI0003',
            'bom_id' => 'B00001',
            'material_id' => 'M00004',
            'quantity' => 40,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_item')->insert([
            'id' => 'BI0004',
            'bom_id' => 'B00002',
            'material_id' => 'M00005',
            'quantity' => 10,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_item')->insert([
            'id' => 'BI0005',
            'bom_id' => 'B00002',
            'material_id' => 'M00006',
            'quantity' => 20,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_item')->insert([
            'id' => 'BI0006',
            'bom_id' => 'B00002',
            'material_id' => 'M00007',
            'quantity' => 20,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_item')->insert([
            'id' => 'BI0007',
            'bom_id' => 'B00003',
            'material_id' => 'M00008',
            'quantity' => 15,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_item')->insert([
            'id' => 'BI0008',
            'bom_id' => 'B00003',
            'material_id' => 'M00009',
            'quantity' => 30,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('bill_item')->insert([
            'id' => 'BI0009',
            'bom_id' => 'B00003',
            'material_id' => 'M00010',
            'quantity' => 30,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
