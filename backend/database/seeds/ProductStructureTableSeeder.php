<?php

use Illuminate\Database\Seeder;

class ProductStructureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_structure')->insert([
            'id' => 'S00001',
            'product_id' => 'P00001',
            'estimated_completion_hours' => 7.5,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure')->insert([
            'id' => 'S00002',
            'product_id' => 'P00002',
            'estimated_completion_hours' => 7.5,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure')->insert([
            'id' => 'S00003',
            'product_id' => 'P00003',
            'estimated_completion_hours' => 7.5,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure')->insert([
            'id' => 'S00004',
            'product_id' => 'P00004',
            'estimated_completion_hours' => 8.5,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
