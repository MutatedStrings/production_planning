<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->insert([
            'id' => 'P00001',
            'name' => 'Hummer Bicycle',
            'product_ref_id' => 14,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('product')->insert([
            'id' => 'P00002',
            'name' => 'Scooty Pep Pedal Pro',
            'product_ref_id' => 15,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('product')->insert([
            'id' => 'P00003',
            'name' => 'Yamaha Apache Pedalman',
            'product_ref_id' => 16,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('product')->insert([
            'id' => 'P00004',
            'name' => 'Fuji Bicycle',
            'product_ref_id' => 17,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
    }
}
