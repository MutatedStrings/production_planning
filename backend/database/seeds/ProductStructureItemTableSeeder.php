<?php

use Illuminate\Database\Seeder;

class ProductStructureItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_structure_item')->insert([
            'id' => 'SI0001',
            'structure_id' => 'S00001',
            'material_id' => 'M00001',
            'quantity' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0002',
            'structure_id' => 'S00001',
            'material_id' => 'M00003',
            'quantity' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0003',
            'structure_id' => 'S00001',
            'material_id' => 'M00004',
            'quantity' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
       
        DB::table('product_structure_item')->insert([
            'id' => 'SI0004',
            'structure_id' => 'S00002',
            'material_id' => 'M00005',
            'quantity' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0005',
            'structure_id' => 'S00002',
            'material_id' => 'M00006',
            'quantity' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0006',
            'structure_id' => 'S00002',
            'material_id' => 'M00007',
            'quantity' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0007',
            'structure_id' => 'S00003',
            'material_id' => 'M00008',
            'quantity' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0008',
            'structure_id' => 'S00003',
            'material_id' => 'M00009',
            'quantity' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0009',
            'structure_id' => 'S00003',
            'material_id' => 'M00010',
            'quantity' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0010',
            'structure_id' => 'S00004',
            'material_id' => 'M00011',
            'quantity' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0011',
            'structure_id' => 'S00004',
            'material_id' => 'M00012',
            'quantity' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('product_structure_item')->insert([
            'id' => 'SI0012',
            'structure_id' => 'S00004',
            'material_id' => 'M00013',
            'quantity' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
