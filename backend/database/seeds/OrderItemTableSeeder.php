<?php

use Illuminate\Database\Seeder;

class OrderItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_item')->insert([
            'id' => 'PO0001',
            'quantity' => 20,
            'product_id' => 'P00001',
            'order_id' => 'C00001',
            'status_id' => 1,
            'order_item_ref_id' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order_item')->insert([
            'id' => 'PO0002',
            'quantity' => 10,
            'product_id' => 'P00002',
            'order_id' => 'C00001',
            'status_id' => 1,
            'order_item_ref_id' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
        
        DB::table('order_item')->insert([
            'id' => 'PO0003',
            'quantity' => 15,
            'product_id' => 'P00003',
            'order_id' => 'C00001',
            'status_id' => 1,
            'order_item_ref_id' => 3,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]);
    }
}
