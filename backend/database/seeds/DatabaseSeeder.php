<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        $this->call('EmployeeTableSeeder');
        $this->call('ProductTableSeeder');
        $this->call('MaterialTableSeeder');
        $this->call('ProductStructureTableSeeder');
        $this->call('OrderStatusTableSeeder');
        $this->call('OrderTableSeeder');
        $this->call('OrderItemStatusTableSeeder');
        $this->call('OrderItemTableSeeder');
        $this->call('BomStatusTableSeeder');
        $this->call('BillOfMaterialTableSeeder');
        $this->call('BillItemTableSeeder');
    }
}
