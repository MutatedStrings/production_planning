<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('employee')->insert([
            'id' => 'E00001',
            'name' => 'John Doe',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin123'),
            'contact_number' => '0777777777',
            'employee_type' => 'ADMIN',
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('employee')->insert([
            'id' => 'E00002',
            'name' => 'Johnny Depp',
            'email' => 'johnny@depp.com',
            'password' => Hash::make('123456'),
            'contact_number' => '0777777777',
            'employee_type' => 'PM',
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('employee')->insert([
            'id' => 'E00003',
            'name' => 'Bob Builder',
            'email' => 'bob@bob.com',
            'password' => Hash::make('123456'),
            'contact_number' => '0777777777',
            'employee_type' => 'PO',
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
        
        DB::table('employee')->insert([
            'id' => 'E00004',
            'name' => 'Tom Gadget',
            'email' => 'tom@tom.com',
            'password' => Hash::make('123456'),
            'contact_number' => '0777777777',
            'employee_type' => 'PI',
             'created_at' => DB::raw('CURRENT_TIMESTAMP'),
             'updated_at' => DB::raw('CURRENT_TIMESTAMP'),
        ]);
    }
}
