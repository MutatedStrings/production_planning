<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBillOfMaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bill_of_material', function(Blueprint $table)
		{
			$table->foreign('status_id', 'FKbill_of_ma85351')->references('id')->on('bom_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('order_item_id', 'FKbill_of_ma968859')->references('id')->on('order_item')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bill_of_material', function(Blueprint $table)
		{
			$table->dropForeign('FKbill_of_ma85351');
			$table->dropForeign('FKbill_of_ma968859');
		});
	}

}
