<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRejectionNoticeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rejection_notice', function(Blueprint $table)
		{
			$table->char('id', 6)->primary();
			$table->char('inspection_id', 6)->index('FKrejection_964920');
			$table->string('rejection_cause', 50);
			$table->string('action', 25)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rejection_notice');
	}

}
