<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeaveTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leave', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 6)->index('FKleave564680');
			$table->date('date');
			$table->char('employee_id', 6)->index('FKleave561240');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leave');
	}

}
