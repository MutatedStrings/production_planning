<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bill_item', function(Blueprint $table)
		{
			$table->char('id', 6)->primary();
			$table->char('bom_id', 6)->index('FKbill_item994148');
			$table->char('material_id', 6)->index('FKbill_item361724');
			$table->integer('quantity');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bill_item');
	}

}
