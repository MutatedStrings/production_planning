<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBillItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bill_item', function(Blueprint $table)
		{
			$table->foreign('material_id', 'FKbill_item361724')->references('id')->on('material')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('bom_id', 'FKbill_item994148')->references('id')->on('bill_of_material')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bill_item', function(Blueprint $table)
		{
			$table->dropForeign('FKbill_item361724');
			$table->dropForeign('FKbill_item994148');
		});
	}

}
