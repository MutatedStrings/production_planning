<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRejectionNoticeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rejection_notice', function(Blueprint $table)
		{
			$table->foreign('inspection_id', 'FKrejection_964920')->references('id')->on('inspection')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rejection_notice', function(Blueprint $table)
		{
			$table->dropForeign('FKrejection_964920');
		});
	}

}
