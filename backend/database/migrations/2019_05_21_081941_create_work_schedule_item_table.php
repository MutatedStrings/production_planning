<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkScheduleItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('work_schedule_item', function(Blueprint $table)
		{
			$table->char('id', 6)->primary();
			$table->timestamp('start')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->dateTime('end')->default('0000-00-00 00:00:00');
			$table->char('order_item_id', 6)->index('FKwork_sched242070');
			$table->char('employee_id', 6)->index('FKwork_sched595789');
			$table->date('date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('work_schedule_item');
	}

}
