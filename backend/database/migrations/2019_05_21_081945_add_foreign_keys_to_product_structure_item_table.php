<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProductStructureItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_structure_item', function(Blueprint $table)
		{
			$table->foreign('material_id', 'FKproduct_st385343')->references('id')->on('material')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('structure_id', 'FKproduct_st88606')->references('id')->on('product_structure')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_structure_item', function(Blueprint $table)
		{
			$table->dropForeign('FKproduct_st385343');
			$table->dropForeign('FKproduct_st88606');
		});
	}

}
