<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrderItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_item', function(Blueprint $table)
		{
			$table->foreign('product_id', 'FKorder_item339094')->references('id')->on('product')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('order_id', 'FKorder_item409001')->references('id')->on('order')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('status_id', 'FKorder_item747616')->references('id')->on('order_item_status')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_item', function(Blueprint $table)
		{
			$table->dropForeign('FKorder_item339094');
			$table->dropForeign('FKorder_item409001');
			$table->dropForeign('FKorder_item747616');
		});
	}

}
