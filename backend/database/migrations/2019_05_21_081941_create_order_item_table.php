<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_item', function(Blueprint $table)
		{
			$table->char('id', 6)->primary();
			$table->integer('quantity');
			$table->char('product_id', 6)->index('FKorder_item339094');
			$table->char('order_id', 6)->index('FKorder_item409001');
			$table->date('completion_date')->nullable();
			$table->integer('status_id')->index('FKorder_item747616');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_item');
	}

}
