<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductStructureItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_structure_item', function(Blueprint $table)
		{
			$table->char('id', 6)->primary();
			$table->char('structure_id', 6)->index('FKproduct_st88606');
			$table->char('material_id', 6)->index('FKproduct_st385343');
			$table->integer('quantity');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_structure_item');
	}

}
