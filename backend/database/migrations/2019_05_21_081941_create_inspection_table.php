<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInspectionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inspection', function(Blueprint $table)
		{
			$table->char('id', 6)->primary();
			$table->date('inspection_date');
			$table->char('inspector_id', 6)->index('FKinspection954246');
			$table->string('inspection_result', 8);
			$table->char('order_item_id', 6)->index('FKinspection86814');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inspection');
	}

}
