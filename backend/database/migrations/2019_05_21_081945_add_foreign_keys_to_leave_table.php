<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLeaveTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('leave', function(Blueprint $table)
		{
			$table->foreign('employee_id', 'FKleave561240')->references('id')->on('employee')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('code', 'FKleave564680')->references('leave_code')->on('leave_type')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('leave', function(Blueprint $table)
		{
			$table->dropForeign('FKleave561240');
			$table->dropForeign('FKleave564680');
		});
	}

}
