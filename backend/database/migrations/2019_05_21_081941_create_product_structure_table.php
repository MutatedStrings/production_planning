<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductStructureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_structure', function(Blueprint $table)
		{
			$table->char('id', 6)->primary();
			$table->char('product_id', 6)->index('FKproduct_st838874');
			$table->decimal('estimated_completion_hours', 4);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_structure');
	}

}
