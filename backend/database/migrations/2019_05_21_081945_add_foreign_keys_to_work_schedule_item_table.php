<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToWorkScheduleItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('work_schedule_item', function(Blueprint $table)
		{
			$table->foreign('order_item_id', 'FKwork_sched242070')->references('id')->on('order_item')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('employee_id', 'FKwork_sched595789')->references('id')->on('employee')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('work_schedule_item', function(Blueprint $table)
		{
			$table->dropForeign('FKwork_sched242070');
			$table->dropForeign('FKwork_sched595789');
		});
	}

}
