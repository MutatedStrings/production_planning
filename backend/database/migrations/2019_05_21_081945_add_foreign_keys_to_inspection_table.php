<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInspectionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inspection', function(Blueprint $table)
		{
			$table->foreign('order_item_id', 'FKinspection86814')->references('id')->on('order_item')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('inspector_id', 'FKinspection954246')->references('id')->on('employee')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inspection', function(Blueprint $table)
		{
			$table->dropForeign('FKinspection86814');
			$table->dropForeign('FKinspection954246');
		});
	}

}
