<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBillOfMaterialTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bill_of_material', function(Blueprint $table)
		{
			$table->char('id', 6)->primary();
			$table->char('order_item_id', 6)->index('FKbill_of_ma968859');
			$table->integer('status_id')->index('FKbill_of_ma85351');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bill_of_material');
	}

}
