# production_planning

Production planning application with a Lumen REST Api backend and React powered frontend.

## Setup

Create .env file in /backend and add database name and credentials.

Create the database tables using the migrate:db command:

```bash
$ php artisan db:migrate
```

To get started with some test info use:

```bash
$ php artisan db:seed
```